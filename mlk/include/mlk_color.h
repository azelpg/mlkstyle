/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_COLOR_H
#define MLK_COLOR_H

typedef struct _mRGB8
{
	uint8_t r,g,b;
}mRGB8;

typedef struct _mRGBd
{
	double r,g,b;
}mRGBd;

typedef struct _mHSVd
{
	double h,s,v;
}mHSVd;

typedef struct _mHSLd
{
	double h,s,l;
}mHSLd;


#ifdef __cplusplus
extern "C" {
#endif

void mHSV_to_RGBd(mRGBd *dst,double h,double s,double v);
void mHSV_to_RGB8(mRGB8 *dst,double h,double s,double v);
uint32_t mHSV_to_RGB8pac(double h,double s,double v);
uint32_t mHSVi_to_RGB8pac(int h,int s,int v);

void mRGBd_to_HSV(mHSVd *dst,double r,double g,double b);
void mRGB8_to_HSV(mHSVd *dst,int r,int g,int b);
void mRGB8pac_to_HSV(mHSVd *dst,uint32_t rgb);

void mHSL_to_RGBd(mRGBd *dst,double h,double s,double l);
void mHSL_to_RGB8(mRGB8 *dst,double h,double s,double l);
uint32_t mHSL_to_RGB8pac(double h,double s,double l);

void mRGBd_to_HSL(mHSLd *dst,double r,double g,double b);
void mRGB8_to_HSL(mHSLd *dst,int r,int g,int b);
void mRGB8pac_to_HSL(mHSLd *dst,uint32_t rgb);

#ifdef __cplusplus
}
#endif

#endif
