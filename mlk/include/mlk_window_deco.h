/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_WINDOW_DECO_H
#define MLK_WINDOW_DECO_H

typedef struct _mWindowDecoInfo mWindowDecoInfo;

struct _mWindowDecoInfo
{
	int width,
		height,
		left,top,right,bottom;
};


enum MWINDOW_DECO_SETWIDTH_TYPE
{
	MWINDOW_DECO_SETWIDTH_TYPE_NORMAL,
	MWINDOW_DECO_SETWIDTH_TYPE_MAXIMIZED,
	MWINDOW_DECO_SETWIDTH_TYPE_FULLSCREEN
};

enum MWINDOW_DECO_UPDATE_TYPE
{
	MWINDOW_DECO_UPDATE_TYPE_RESIZE,
	MWINDOW_DECO_UPDATE_TYPE_ACTIVE,
	MWINDOW_DECO_UPDATE_TYPE_TITLE
};


#ifdef __cplusplus
extern "C" {
#endif

void mWindowDeco_setWidth(mWindow *p,int left,int top,int right,int bottom);
void mWindowDeco_updateBox(mWindow *p,int x,int y,int w,int h);

#ifdef __cplusplus
}
#endif

#endif
