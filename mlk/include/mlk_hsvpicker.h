/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_HSVPICKER_H
#define MLK_HSVPICKER_H

#define MLK_HSVPICKER(p)  ((mHSVPicker *)(p))
#define MLK_HSVPICKER_DEF mWidget wg; mHSVPickerData hsv;

typedef struct
{
	mPixbuf *img;
	mRgbCol col;
	int cur_sv_x,
		cur_sv_y,
		cur_hue,
		cur_hue_y,
		fgrab;
}mHSVPickerData;

struct _mHSVPicker
{
	mWidget wg;
	mHSVPickerData hsv;
};

enum MHSVPICKER_NOTIFY
{
	MHSVPICKER_N_CHANGE_HUE,
	MHSVPICKER_N_CHANGE_SV
};


#ifdef __cplusplus
extern "C" {
#endif

mHSVPicker *mHSVPickerNew(mWidget *parent,int size,uint32_t fstyle,int height);
mHSVPicker *mHSVPickerCreate(mWidget *parent,int id,uint32_t margin_pack,uint32_t fstyle,int height);

void mHSVPickerDestroy(mWidget *p);
void mHSVPickerHandle_draw(mWidget *p,mPixbuf *pixbuf);
int mHSVPickerHandle_event(mWidget *wg,mEvent *ev);

mRgbCol mHSVPickerGetRGBColor(mHSVPicker *p);
void mHSVPickerGetHSVColor(mHSVPicker *p,double *dst);

void mHSVPickerSetHue(mHSVPicker *p,int hue);
void mHSVPickerSetSV(mHSVPicker *p,double s,double v);
void mHSVPickerSetHSVColor(mHSVPicker *p,double h,double s,double v);
void mHSVPickerSetRGBColor(mHSVPicker *p,mRgbCol col);

#ifdef __cplusplus
}
#endif

#endif
