/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_FONTBUTTON_H
#define MLK_FONTBUTTON_H

#include "mlk_button.h"
#include "mlk_fontinfo.h"
#include "mlk_sysdlg.h"

#define MLK_FONTBUTTON(p)  ((mFontButton *)(p))
#define MLK_FONTBUTTON_DEF MLK_BUTTON_DEF mFontButtonData fbt;

typedef struct
{
	mFontInfo info;
	uint32_t flags;
}mFontButtonData;

struct _mFontButton
{
	MLK_BUTTON_DEF
	mFontButtonData fbt;
};

enum MFONTBUTTON_STYLE
{
	MFONTBUTTON_S_ALL_INFO = 1<<0
};

enum MFONTBUTTON_NOTIFY
{
	MFONTBUTTON_N_CHANGE_FONT
};


#ifdef __cplusplus
extern "C" {
#endif

mFontButton *mFontButtonNew(mWidget *parent,int size,uint32_t fstyle);
mFontButton *mFontButtonCreate(mWidget *parent,int id,uint32_t flayout,uint32_t margin_pack,uint32_t fstyle);

void mFontButtonDestroy(mWidget *p);

void mFontButtonSetFlags(mFontButton *p,uint32_t flags);
void mFontButtonSetInfo_text(mFontButton *p,const char *text);
void mFontButtonGetInfo_text(mFontButton *p,mStr *str);
void mFontButtonSetInfo(mFontButton *p,const mFontInfo *src);
void mFontButtonGetInfo(mFontButton *p,mFontInfo *dst);

#ifdef __cplusplus
}
#endif

#endif
