/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_ICONBAR_H
#define MLK_ICONBAR_H

#define MLK_ICONBAR(p)  ((mIconBar *)(p))
#define MLK_ICONBAR_DEF mWidget wg;	mIconBarData ib;

typedef struct
{
	mImageList *imglist;
	struct _mIconBarItem *item_sel;
	mTooltip *tooltip;
	mList list;
	uint32_t fstyle;
	int autowrap_width,
		padding;
	uint16_t trgroup;
	uint8_t press;
}mIconBarData;

struct _mIconBar
{
	mWidget wg;
	mIconBarData ib;
};

enum MICONBAR_STYLE
{
	MICONBAR_S_TOOLTIP    = 1<<0,
	MICONBAR_S_AUTO_WRAP  = 1<<1,
	MICONBAR_S_VERT       = 1<<2,
	MICONBAR_S_SEP_BOTTOM = 1<<3,
	MICONBAR_S_BUTTON_FRAME = 1<<4,
	MICONBAR_S_DESTROY_IMAGELIST = 1<<5
};

enum MICONBAR_ITEM_FLAGS
{
	MICONBAR_ITEMF_BUTTON = 0,
	MICONBAR_ITEMF_CHECKBUTTON = 1<<0,
	MICONBAR_ITEMF_CHECKGROUP  = 1<<1,
	MICONBAR_ITEMF_SEP      = 1<<2,
	MICONBAR_ITEMF_DROPDOWN = 1<<3,
	MICONBAR_ITEMF_WRAP     = 1<<4,
	MICONBAR_ITEMF_DISABLE  = 1<<5,
	MICONBAR_ITEMF_CHECKED  = 1<<6
};


#ifdef __cplusplus
extern "C" {
#endif

mIconBar *mIconBarNew(mWidget *parent,int size,uint32_t fstyle);
mIconBar *mIconBarCreate(mWidget *parent,int id,uint32_t flayout,uint32_t margin_pack,uint32_t fstyle);

void mIconBarDestroy(mWidget *p);
void mIconBarHandle_calcHint(mWidget *p);
void mIconBarHandle_draw(mWidget *p,mPixbuf *pixbuf);
int mIconBarHandle_event(mWidget *wg,mEvent *ev);

void mIconBarSetPadding(mIconBar *p,int padding);
void mIconBarSetImageList(mIconBar *p,mImageList *img);
void mIconBarSetTooltipTrGroup(mIconBar *p,uint16_t gid);

void mIconBarDeleteAll(mIconBar *p);
void mIconBarAdd(mIconBar *p,int id,int img,int tooltip,uint32_t flags);
void mIconBarAddSep(mIconBar *p);

void mIconBarSetCheck(mIconBar *p,int id,int type);
mlkbool mIconBarIsChecked(mIconBar *p,int id);
void mIconBarSetEnable(mIconBar *p,int id,int type);

mlkbool mIconBarGetItemBox(mIconBar *p,int id,mBox *box);

#ifdef __cplusplus
}
#endif

#endif
