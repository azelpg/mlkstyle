/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_CHARSET_H
#define MLK_CHARSET_H

typedef void * mIconv;

typedef struct _mIconvCallback
{
	int (*in)(void *buf,int size,void *param);
	int (*out)(void *buf,int size,void *param);
}mIconvCallback;


#ifdef __cplusplus
extern "C" {
#endif

void mInitLocale(void);
const char *mGetLocaleCharset(void);
mlkbool mLocaleCharsetIsUTF8(void);

mlkbool mIconvOpen(mIconv *p,const char *from,const char *to);
void mIconvClose(mIconv p);
char *mIconvConvert(mIconv p,const void *src,int srclen,int *dstlen,int nullsize);
void mIconvConvert_outfunc(mIconv p,const void *src,int srclen,int (*func)(void *buf,int size,void *param),void *param);
mlkbool mIconvConvert_callback(mIconv p,int inbufsize,int outbufsize,mIconvCallback *cb,void *param);

void *mConvertCharset(const void *src,int srclen,const char *from,const char *to,int *dstlen,int nullsize);

char *mUTF8toLocale(const char *str,int len,int *dstlen);
char *mLocaletoUTF8(const char *str,int len,int *dstlen);
mlkuchar *mLocaletoUTF32(const char *src,int len,int *dstlen);

char *mWidetoUTF8(const void *src,int len,int *dstlen);
mlkuchar *mWidetoUTF32(const void *src,int len,int *dstlen);
void *mLocaletoWide(const char *src,int len,int *dstlen);

void mPutUTF8(void *fp,const char *str,int len);
void mPutUTF8_stdout(const char *str);
void mPutUTF8_format_stdout(const char *format,const char *str);
void mPutUTF8_stderr(const char *str);

#ifdef __cplusplus
}
#endif

#endif
