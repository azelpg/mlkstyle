/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_LISTVIEWPAGE_H
#define MLK_LISTVIEWPAGE_H

#include "mlk_scrollview.h"

#define MLK_LISTVIEWPAGE(p)  ((mListViewPage *)(p))
#define MLK_LISTVIEWPAGE_DEF MLK_SCROLLVIEWPAGE_DEF mListViewPageData lvp;

#define MLK_LISTVIEWPAGE_ITEM_PADDING_X 3
#define MLK_LISTVIEWPAGE_ITEM_PADDING_Y 2
#define MLK_LISTVIEWPAGE_CHECKBOX_SIZE  13
#define MLK_LISTVIEWPAGE_CHECKBOX_SPACE_RIGHT 4
#define MLK_LISTVIEWPAGE_ICON_SPACE_RIGHT     3

typedef struct
{
	mCIManager *manager;
	mImageList *imglist;
	mListHeader *header;
	mWidget *widget_send;
	uint32_t fstyle,
		fstyle_listview;
	int item_height,
		header_height;
}mListViewPageData;

struct _mListViewPage
{
	MLK_SCROLLVIEWPAGE_DEF
	mListViewPageData lvp;
};

enum MLISTVIEWPAGE_STYLE
{
	MLISTVIEWPAGE_S_POPUP = 1
};

enum MLISTVIEWPAGE_NOTIFY
{
	MLISTVIEWPAGE_N_POPUP_QUIT = 10000
};


mListViewPage *mListViewPageNew(mWidget *parent,int size,
	uint32_t fstyle,uint32_t fstyle_listview,
	mCIManager *manager,mWidget *send);

void mListViewPageDestroy(mWidget *wg);
int mListViewPageHandle_event(mWidget *wg,mEvent *ev);

void mListViewPage_setImageList(mListViewPage *p,mImageList *img);
void mListViewPage_scrollToFocus(mListViewPage *p,int dir,int margin_num);
int mListViewPage_getItemY(mListViewPage *p,mColumnItem *pi);

#endif
