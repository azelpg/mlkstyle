/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_LABEL_H
#define MLK_LABEL_H

#define MLK_LABEL(p)  ((mLabel *)(p))
#define MLK_LABEL_DEF mWidget wg; mLabelData lb;

typedef struct
{
	mWidgetLabelText txt;
	uint32_t fstyle;
}mLabelData;

struct _mLabel
{
	mWidget wg;
	mLabelData lb;
};

enum MLABEL_STYLE
{
	MLABEL_S_COPYTEXT = 1<<0,
	MLABEL_S_RIGHT  = 1<<1,
	MLABEL_S_CENTER = 1<<2,
	MLABEL_S_BOTTOM = 1<<3,
	MLABEL_S_MIDDLE = 1<<4,
	MLABEL_S_BORDER = 1<<5
};


#ifdef __cplusplus
extern "C" {
#endif

mLabel *mLabelNew(mWidget *parent,int size,uint32_t fstyle);
mLabel *mLabelCreate(mWidget *parent,uint32_t flayout,uint32_t margin_pack,uint32_t fstyle,const char *text);
void mLabelDestroy(mWidget *p);

void mLabelHandle_calcHint(mWidget *p);
void mLabelHandle_draw(mWidget *p,mPixbuf *pixbuf);

void mLabelSetText(mLabel *p,const char *text);
void mLabelSetText_copy(mLabel *p,const char *text);
void mLabelSetText_int(mLabel *p,int val);
void mLabelSetText_floatint(mLabel *p,int val,int dig);

#ifdef __cplusplus
}
#endif

#endif
