/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_GUICOL_H
#define MLK_GUICOL_H

enum MGUICOL
{
	MGUICOL_WHITE,

	MGUICOL_FACE,
	MGUICOL_FACE_DISABLE,
	MGUICOL_FACE_SELECT,
	MGUICOL_FACE_SELECT_UNFOCUS,
	MGUICOL_FACE_SELECT_LIGHT,
	MGUICOL_FACE_TEXTBOX,
	MGUICOL_FACE_DARK,

	MGUICOL_FRAME,
	MGUICOL_FRAME_BOX,
	MGUICOL_FRAME_FOCUS,
	MGUICOL_FRAME_DISABLE,

	MGUICOL_GRID,

	MGUICOL_TEXT,
	MGUICOL_TEXT_DISABLE,
	MGUICOL_TEXT_SELECT,

	MGUICOL_BUTTON_FACE,
	MGUICOL_BUTTON_FACE_PRESS,
	MGUICOL_BUTTON_FACE_DEFAULT,

	MGUICOL_SCROLLBAR_FACE,
	MGUICOL_SCROLLBAR_GRIP,

	MGUICOL_MENU_FACE,
	MGUICOL_MENU_FRAME,
	MGUICOL_MENU_SEP,
	MGUICOL_MENU_SELECT,
	MGUICOL_MENU_TEXT,
	MGUICOL_MENU_TEXT_DISABLE,
	MGUICOL_MENU_TEXT_SHORTCUT,

	MGUICOL_TOOLTIP_FACE,
	MGUICOL_TOOLTIP_FRAME,
	MGUICOL_TOOLTIP_TEXT,

	MGUICOL_NUM
};

#define MGUICOL_RGB(name) mGuiCol_getRGB(MGUICOL_ ## name)
#define MGUICOL_PIX(name) mGuiCol_getPix(MGUICOL_ ## name)


#ifdef __cplusplus
extern "C" {
#endif

mRgbCol mGuiCol_getRGB(int no);
mPixCol mGuiCol_getPix(int no);

void mGuiCol_changeColor(int no,uint32_t col);
void mGuiCol_setRGBColor(int no,uint32_t col);
void mGuiCol_setDefaultRGB(void);
void mGuiCol_RGBtoPix_all(void);

#ifdef __cplusplus
}
#endif

#endif
