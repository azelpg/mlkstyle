/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_FONT_H
#define MLK_FONT_H

typedef void (*mFuncFontSetPixelMono)(int x,int y,void *param);
typedef void (*mFuncFontSetPixelGray)(int x,int y,int a,void *param);
typedef void (*mFuncFontSetPixelLCD)(int x,int y,int ra,int ga,int ba,void *param);
typedef void (*mFuncFontDrawUnderline)(int x,int y,int w,int h,void *param);

struct _mFontDrawInfo
{
	mFuncFontSetPixelMono setpix_mono;
	mFuncFontSetPixelGray setpix_gray;
	mFuncFontSetPixelLCD setpix_lcd;
	mFuncFontDrawUnderline draw_underline;
};

typedef struct
{
	mPixbuf *pixbuf;
	mRgbCol rgbcol;
	mPixCol pixcol;
}mFontDrawParam_pixbuf;


#ifdef __cplusplus
extern "C" {
#endif

mFontSystem *mFontSystem_init(void);
void mFontSystem_finish(mFontSystem *p);

mRgbCol mFontBlendColor_gray(mRgbCol bgcol,mRgbCol fgcol,int a);
mRgbCol mFontBlendColor_lcd(mRgbCol bgcol,mRgbCol fgcol,int ra,int ga,int ba);

int mFontGetFilename_fromFamily(char **dst,const char *family,const char *style);

/* mFont */

void mFontFree(mFont *p);

mFont *mFontCreate_file(mFontSystem *sys,const char *filename,int index);
mFont *mFontCreate(mFontSystem *sys,const mFontInfo *info);
mFont *mFontCreate_default(mFontSystem *sys);
mFont *mFontCreate_family(mFontSystem *sys,const char *family,const char *style);
mFont *mFontCreate_raw(mFontSystem *sys,const mFontInfo *info);
mFont *mFontCreate_text(mFontSystem *sys,const char *text);
mFont *mFontCreate_text_size(mFontSystem *sys,const char *text,double size);

int mFontGetListNames(mFontSystem *sys,const char *filename,
	void (*func)(int index,const char *name,void *param),void *param);

int mFontGetHeight(mFont *p);
int mFontGetLineHeight(mFont *p);
int mFontGetVertHeight(mFont *p);
int mFontGetAscender(mFont *p);
mlkbool mFontIsHaveVert(mFont *p);

void mFontSetSize(mFont *p,double size);
void mFontSetSize_pt(mFont *p,double size,int dpi);
void mFontSetSize_pixel(mFont *p,int size);
void mFontSetSize_fromText(mFont *p,const char *text);
void mFontSetInfoEx(mFont *p,const mFontInfo *info);

int mFontGetTextWidth(mFont *p,const char *text,int len);
int mFontGetTextWidth_hotkey(mFont *p,const char *text,int len);
int mFontGetTextWidth_utf32(mFont *p,const mlkuchar *text,int len);
int mFontGetCharWidth_utf32(mFont *p,mlkuchar code);

void mFontDrawText(mFont *p,int x,int y,const char *text,int len,mFontDrawInfo *info,void *param);
void mFontDrawText_hotkey(mFont *p,int x,int y,const char *text,int len,mFontDrawInfo *info,void *param);
void mFontDrawText_utf32(mFont *p,int x,int y,const mlkuchar *text,int len,mFontDrawInfo *info,void *param);

/* pixbuf */
mFontDrawInfo *mFontGetDrawInfo_pixbuf(void);
void mFontSetDrawParam_pixbuf(mFontDrawParam_pixbuf *p,mPixbuf *pixbuf,mRgbCol col);
void mFontDrawText_pixbuf(mFont *p,mPixbuf *img,int x,int y,const char *text,int len,mRgbCol col);
void mFontDrawText_pixbuf_hotkey(mFont *p,mPixbuf *img,int x,int y,const char *text,int len,mRgbCol col);
void mFontDrawText_pixbuf_utf32(mFont *p,mPixbuf *img,int x,int y,const mlkuchar *text,int len,mRgbCol col);

#ifdef __cplusplus
}
#endif

#endif
