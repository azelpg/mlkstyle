/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_TRANSLATION_H
#define MLK_TRANSLATION_H

typedef struct _mTranslation mTranslation;

#ifdef __cplusplus
extern "C" {
#endif

void mTranslationFree(mTranslation *p);

mTranslation *mTranslationLoadDefault(const void *buf);

mTranslation *mTranslationLoadFile(const char *filename,mlkbool locale);
mTranslation *mTranslationLoadFile_lang(const char *path,const char *lang,const char *prefix);

mlkbool mTranslationSetGroup(mTranslation *p,uint16_t id);
const char *mTranslationGetText(mTranslation *p,uint16_t id);
const char *mTranslationGetText2(mTranslation *p,uint16_t groupid,uint16_t itemid);

#ifdef __cplusplus
}
#endif

#endif
