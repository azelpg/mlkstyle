/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_FILELISTVIEW_H
#define MLK_FILELISTVIEW_H

#include "mlk_listview.h"

#define MLK_FILELISTVIEW(p)  ((mFileListView *)(p))
#define MLK_FILELISTVIEW_DEF MLK_LISTVIEW_DEF mFileListViewData flv;

typedef struct
{
	mStr strDir,
		strFilter;
	uint32_t fstyle;
	uint8_t sort_type,
		sort_rev;
}mFileListViewData;

struct _mFileListView
{
	MLK_LISTVIEW_DEF
	mFileListViewData flv;
};

enum MFILELISTVIEW_STYLE
{
	MFILELISTVIEW_S_MULTI_SEL = 1<<0,
	MFILELISTVIEW_S_ONLY_DIR  = 1<<1,
	MFILELISTVIEW_S_SHOW_HIDDEN_FILES = 1<<2
};

enum MFILELISTVIEW_NOTIFY
{
	MFILELISTVIEW_N_SELECT_FILE,
	MFILELISTVIEW_N_DBLCLK_FILE,
	MFILELISTVIEW_N_CHANGE_DIR
};

enum MFILELISTVIEW_FILETYPE
{
	MFILELISTVIEW_FILETYPE_NONE,
	MFILELISTVIEW_FILETYPE_FILE,
	MFILELISTVIEW_FILETYPE_DIR
};

enum MFILELISTVIEW_SORTTYPE
{
	MFILELISTVIEW_SORTTYPE_FILENAME,
	MFILELISTVIEW_SORTTYPE_FILESIZE,
	MFILELISTVIEW_SORTTYPE_MODIFY
};


#ifdef __cplusplus
extern "C" {
#endif

mFileListView *mFileListViewNew(mWidget *parent,int size,uint32_t fstyle);

void mFileListViewDestroy(mWidget *p);
int mFileListViewHandle_event(mWidget *wg,mEvent *ev);

void mFileListView_setDirectory(mFileListView *p,const char *path);
void mFileListView_setFilter(mFileListView *p,const char *filter);
void mFileListView_setFilter_ext(mFileListView *p,const char *ext);
void mFileListView_setShowHiddenFiles(mFileListView *p,int type);
void mFileListView_setSortType(mFileListView *p,int type,mlkbool rev);

mlkbool mFileListView_isShowHiddenFiles(mFileListView *p);

void mFileListView_updateList(mFileListView *p);
void mFileListView_changeDir_parent(mFileListView *p);

int mFileListView_getSelectFileName(mFileListView *p,mStr *str,mlkbool fullpath);
int mFileListView_getSelectMultiName(mFileListView *p,mStr *str);

#ifdef __cplusplus
}
#endif

#endif
