/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_CURSOR_H
#define MLK_CURSOR_H

enum MCURSOR_TYPE
{
	MCURSOR_TYPE_NONE,
	MCURSOR_TYPE_DEFAULT,
	MCURSOR_TYPE_WE_ARROW,
	MCURSOR_TYPE_NS_ARROW,
	MCURSOR_TYPE_NWSE_ARROW,
	MCURSOR_TYPE_NESW_ARROW,
	MCURSOR_TYPE_RESIZE_W,
	MCURSOR_TYPE_RESIZE_E,
	MCURSOR_TYPE_RESIZE_N,
	MCURSOR_TYPE_RESIZE_S,
	MCURSOR_TYPE_RESIZE_NW,
	MCURSOR_TYPE_RESIZE_NE,
	MCURSOR_TYPE_RESIZE_SW,
	MCURSOR_TYPE_RESIZE_SE,
	MCURSOR_TYPE_RESIZE_COL,
	MCURSOR_TYPE_RESIZE_ROW,

	MCURSOR_TYPE_MAX
};

enum MCURSOR_RESIZE_FLAGS
{
	MCURSOR_RESIZE_TOP    = 1,
	MCURSOR_RESIZE_BOTTOM = 2,
	MCURSOR_RESIZE_LEFT   = 4,
	MCURSOR_RESIZE_RIGHT  = 8,
	MCURSOR_RESIZE_TOP_LEFT = 5,
	MCURSOR_RESIZE_TOP_RIGHT = 9,
	MCURSOR_RESIZE_BOTTOM_LEFT = 6,
	MCURSOR_RESIZE_BOTTOM_RIGHT = 10
};

#ifdef __cplusplus
extern "C" {
#endif

void mCursorFree(mCursor p);
mCursor mCursorLoad(const char *name);
mCursor mCursorLoad_names(const char *names);
mCursor mCursorLoad_type(int type);
mCursor mCursorCreate1bit(const uint8_t *buf);
mCursor mCursorCreateEmpty(void);
mCursor mCursorCreateRGBA(int width,int height,int hotspot_x,int hotspot_y,const uint8_t *buf);
mCursor mCursorLoadFile_png(const char *filename,int hotspot_x,int hotspot_y);

int mCursorGetType_resize(int flags);

mCursor mCursorCache_getCursor_type(int type);
void mCursorCache_addCursor(mCursor cur);
void mCursorCache_release(mCursor cur);

#ifdef __cplusplus
}
#endif

#endif
