/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_IMGBUTTON_H
#define MLK_IMGBUTTON_H

#include "mlk_button.h"

#define MLK_IMGBUTTON(p)  ((mImgButton *)(p))
#define MLK_IMGBUTTON_DEF MLK_BUTTON_DEF mImgButtonData bib;

typedef struct
{
	const uint8_t *imgbuf;
	int type,
		imgw,
		imgh;
}mImgButtonData;

struct _mImgButton
{
	MLK_BUTTON_DEF
	mImgButtonData bib;
};


enum MIMGBUTTON_TYPE
{
	MIMGBUTTON_TYPE_1BIT_TP_TEXT,
	MIMGBUTTON_TYPE_1BIT_WHITE_BLACK,
	MIMGBUTTON_TYPE_2BIT_BLACK_TP_WHITE
};


#ifdef __cplusplus
extern "C" {
#endif

mImgButton *mImgButtonNew(mWidget *parent,int size,uint32_t fstyle);
mImgButton *mImgButtonCreate(mWidget *parent,int id,uint32_t flayout,uint32_t margin_pack,uint32_t fstyle);

void mImgButtonDestroy(mWidget *p);
void mImgButtonHandle_calcHint(mWidget *p);
void mImgButtonHandle_draw(mWidget *p,mPixbuf *pixbuf);

void mImgButton_setBitImage(mImgButton *p,int type,const uint8_t *img,int width,int height);

#ifdef __cplusplus
}
#endif

#endif
