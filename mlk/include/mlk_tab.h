/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_TAB_H
#define MLK_TAB_H

#define MLK_TAB(p)     ((mTab *)(p))
#define MLK_TABITEM(p) ((mTabItem *)(p))
#define MLK_TAB_DEF    mWidget wg; mTabData tab;

typedef struct
{
	mListItem i;
	char *text;
	int width,pos;
	uint32_t flags;
	intptr_t param;
}mTabItem;

typedef struct
{
	uint32_t fstyle;
	mList list;

	mTabItem *focusitem;
}mTabData;

struct _mTab
{
	mWidget wg;
	mTabData tab;
};


enum MTAB_STYLE
{
	MTAB_S_TOP    = 1<<0,
	MTAB_S_BOTTOM = 1<<1,
	MTAB_S_LEFT   = 1<<2,
	MTAB_S_RIGHT  = 1<<3,
	MTAB_S_HAVE_SEP = 1<<4,
	MTAB_S_SHORTEN = 1<<5
};

enum MTAB_NOTIFY
{
	MTAB_N_CHANGE_SEL
};

enum MTABITEM_FLAGS
{
	MTABITEM_F_COPYTEXT = 1<<0
};


#ifdef __cplusplus
extern "C" {
#endif

mTab *mTabNew(mWidget *parent,int size,uint32_t fstyle);
mTab *mTabCreate(mWidget *parent,int id,uint32_t flayout,uint32_t margin_pack,uint32_t fstyle);

void mTabDestroy(mWidget *p);
void mTabHandle_calcHint(mWidget *p);
void mTabHandle_draw(mWidget *p,mPixbuf *pixbuf);
int mTabHandle_event(mWidget *wg,mEvent *ev);
void mTabHandle_resize(mWidget *wg);

void mTabAddItem(mTab *p,const char *text,int w,uint32_t flags,intptr_t param);
void mTabAddItem_text_static(mTab *p,const char *text);
mlkbool mTabDelItem_atIndex(mTab *p,int index);

int mTabGetSelItemIndex(mTab *p);
mTabItem *mTabGetItem_atIndex(mTab *p,int index);
intptr_t mTabGetItemParam_atIndex(mTab *p,int index);

void mTabSetSel_atIndex(mTab *p,int index);
void mTabSetHintSize(mTab *p);

#ifdef __cplusplus
}
#endif

#endif
