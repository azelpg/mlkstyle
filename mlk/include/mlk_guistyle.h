/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_GUISTYLE_H
#define MLK_GUISTYLE_H

#include "mlk_guicol.h"
#include "mlk_fontinfo.h"

typedef struct _mGuiStyleInfo
{
	mRgbCol col[MGUICOL_NUM];
	mFontInfo fontinfo;
	mStr strIcontheme;
}mGuiStyleInfo;

void mGuiStyle_free(mGuiStyleInfo *p);
void mGuiStyle_load(const char *filename,mGuiStyleInfo *info);
void mGuiStyle_loadColor(const char *filename);
void mGuiStyle_save(const char *filename,mGuiStyleInfo *info);

void mGuiStyle_getCurrentInfo(mGuiStyleInfo *info);
void mGuiStyle_getCurrentColor(mGuiStyleInfo *info);
void mGuiStyle_setColor(mGuiStyleInfo *info);

#endif
