/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_COLORBUTTON_H
#define MLK_COLORBUTTON_H

#include "mlk_button.h"

#define MLK_COLORBUTTON(p)  ((mColorButton *)(p))
#define MLK_COLORBUTTON_DEF MLK_BUTTON_DEF mColorButtonData colbtt;

typedef struct
{
	uint32_t fstyle;
	mRgbCol col;
}mColorButtonData;

struct _mColorButton
{
	MLK_BUTTON_DEF
	mColorButtonData colbtt;
};

enum MCOLORBUTTON_STYLE
{
	MCOLORBUTTON_S_DIALOG = 1<<0
};

enum MCOLORBUTTON_NOTIFY
{
	MCOLORBUTTON_N_PRESS
};


#ifdef __cplusplus
extern "C" {
#endif

mColorButton *mColorButtonNew(mWidget *parent,int size,uint32_t fstyle);
mColorButton *mColorButtonCreate(mWidget *parent,int id,
	uint32_t flayout,uint32_t margin_pack,uint32_t fstyle,mRgbCol col);
void mColorButtonDestroy(mWidget *p);

void mColorButtonHandle_draw(mWidget *p,mPixbuf *pixbuf);

mRgbCol mColorButtonGetColor(mColorButton *p);
void mColorButtonSetColor(mColorButton *p,mRgbCol col);

#ifdef __cplusplus
}
#endif

#endif
