/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_COLUMNITEM_H
#define MLK_COLUMNITEM_H

#define MLK_COLUMNITEM(p)  ((mColumnItem *)(p))

typedef struct _mColumnItemDrawInfo mColumnItemDrawInfo;
typedef int (*mFuncColumnItemDraw)(mPixbuf *pixbuf,mColumnItem *item,mColumnItemDrawInfo *info);

struct _mColumnItem
{
	mListItem i;

	char *text;
	uint8_t *text_col;
	int icon,
		text_width;
	uint32_t flags;
	intptr_t param;
	mFuncColumnItemDraw draw;
};

struct _mColumnItemDrawInfo
{
	mWidget *widget;
	mBox box;
	int column;
	uint32_t flags;
};


enum MCOLUMNITEM_FLAGS
{
	MCOLUMNITEM_F_SELECTED = 1<<0,
	MCOLUMNITEM_F_CHECKED  = 1<<1,
	MCOLUMNITEM_F_HEADER   = 1<<2,
	MCOLUMNITEM_F_COPYTEXT = 1<<3
};

enum MCOLUMNITEM_DRAW_FLAGS
{
	MCOLUMNITEM_DRAWF_DISABLED = 1<<0,
	MCOLUMNITEM_DRAWF_FOCUSED  = 1<<1,
	MCOLUMNITEM_DRAWF_ITEM_SELECTED = 1<<2,
	MCOLUMNITEM_DRAWF_ITEM_FOCUS = 1<<3
};


#ifdef __cplusplus
extern "C" {
#endif

void mColumnItemHandle_destroyItem(mList *list,mListItem *item);
int mColumnItem_getColText(mColumnItem *p,int col,char **pptop);
void mColumnItem_setColText(mColumnItem *p,int col,const char *text);

int mColumnItem_sortFunc_strcmp(mListItem *item1,mListItem *item2,void *param);

#ifdef __cplusplus
}
#endif

#endif
