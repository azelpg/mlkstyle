/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_IMAGELIST_H
#define MLK_IMAGELIST_H

struct _mImageList
{
	uint8_t *buf;
	int w,h,
		pitch,
		eachw,
		num;
};

enum MIMAGELIST_PUT_FLAGS
{
	MIMAGELIST_PUTF_DISABLE = 1<<0
};


#ifdef __cplusplus
extern "C" {
#endif

void mImageListFree(mImageList *p);

mImageList *mImageListNew(int w,int h,int eachw);
mImageList *mImageListLoadPNG(const char *filename,int eachw);
mImageList *mImageListLoadPNG_buf(const uint8_t *buf,int bufsize,int eachw);
mImageList *mImageListCreate_1bit_mono(const uint8_t *buf,int w,int h,int eachw);
mImageList *mImageListCreate_1bit_textcol(const uint8_t *buf,int w,int h,int eachw);

mlkbool mImageListLoad_icontheme(mImageList *p,int pos,mIconTheme theme,const char *names,int iconsize);
mImageList *mImageListCreate_icontheme(mIconTheme theme,const char *names,int iconsize);

void mImageListClear(mImageList *p);
void mImageListSetTransparentColor(mImageList *p,mRgbCol col);
void mImageListReplaceTextColor_mono(mImageList *p);

void mImageListPutPixbuf(mImageList *p,mPixbuf *pixbuf,int dx,int dy,int index,uint32_t flags);
void mImageListSetOne_imagebuf(mImageList *p,int index,mImageBuf *src);

#ifdef __cplusplus
}
#endif

#endif
