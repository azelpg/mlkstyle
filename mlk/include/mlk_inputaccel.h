/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_INPUTACCEL_H
#define MLK_INPUTACCEL_H

#define MLK_INPUTACCEL(p)  ((mInputAccel *)(p))
#define MLK_INPUTACCEL_DEF mWidget wg; mInputAccelData ia;

typedef struct
{
	mStr str;
	uint32_t fstyle,
		key;
}mInputAccelData;

struct _mInputAccel
{
	mWidget wg;
	mInputAccelData ia;
};


enum MINPUTACCEL_STYLE
{
	MINPUTACCEL_S_NOTIFY_CHANGE = 1<<0,
	MINPUTACCEL_S_NOTIFY_ENTER = 1<<1
};

enum MINPUTACCEL_NOTIFY
{
	MINPUTACCEL_N_CHANGE,
	MINPUTACCEL_N_ENTER
};


#ifdef __cplusplus
extern "C" {
#endif

mInputAccel *mInputAccelNew(mWidget *parent,int size,uint32_t fstyle);
mInputAccel *mInputAccelCreate(mWidget *parent,int id,uint32_t flayout,uint32_t margin_pack,uint32_t fstyle);

void mInputAccelDestroy(mWidget *p);
void mInputAccelHandle_calcHint(mWidget *p);
void mInputAccelHandle_draw(mWidget *p,mPixbuf *pixbuf);
int mInputAccelHandle_event(mWidget *wg,mEvent *ev);

uint32_t mInputAccelGetKey(mInputAccel *p);
void mInputAccelSetKey(mInputAccel *p,uint32_t key);

#ifdef __cplusplus
}
#endif

#endif
