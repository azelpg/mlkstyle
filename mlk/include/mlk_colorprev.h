/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_COLORPREV_H
#define MLK_COLORPREV_H

#define MLK_COLORPREV(p)  ((mColorPrev *)(p))
#define MLK_COLORPREV_DEF mWidget wg; mColorPrevData cp;

typedef struct
{
	uint32_t fstyle,rgbcol;
}mColorPrevData;

struct _mColorPrev
{
	mWidget wg;
	mColorPrevData cp;
};

enum MCOLORPREV_STYLE
{
	MCOLORPREV_S_FRAME = 1<<0
};


#ifdef __cplusplus
extern "C" {
#endif

mColorPrev *mColorPrevNew(mWidget *parent,int size,uint32_t fstyle);
mColorPrev *mColorPrevCreate(mWidget *parent,int w,int h,uint32_t margin_pack,uint32_t fstyle,mRgbCol col);
void mColorPrevDestroy(mWidget *p);

void mColorPrevHandle_draw(mWidget *p,mPixbuf *pixbuf);

void mColorPrevSetColor(mColorPrev *p,mRgbCol col);

#ifdef __cplusplus
}
#endif

#endif
