/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * mIconTheme
 *****************************************/

#include <string.h>

#include "mlk.h"
#include "mlk_icontheme.h"
#include "mlk_str.h"
#include "mlk_string.h"
#include "mlk_list.h"
#include "mlk_iniread.h"
#include "mlk_file.h"
#include "mlk_imagebuf.h"
#include "mlk_loadimage.h"
#include "mlk_util.h"


//---------------------

#define _THEMEITEM(p) ((_theme_item *)(p))
#define _DIRITEM(p)   ((_theme_diritem *)(p))

typedef struct __theme_item _theme_item;

/* サブディレクトリアイテム */

typedef struct
{
	mListItem i;
	char *path;		//ディレクトリの相対パス
	uint32_t hash;	//パスのハッシュ値
	int size;		//アイコンの画像サイズ
}_theme_diritem;

/* [テーマアイテム]
 *
 * テーマは他のテーマを継承する場合があるため、ツリー上になっている。
 * ここでは、リストアイテムとして格納し、先頭がルート、以降は子テーマ。 */

struct __theme_item
{
	mListItem i;
	mList dirlist;			//サブディレクトリ リスト
	_theme_item *parent;	//親のテーマアイテム (NULL でルート)
	char *path;				//ディレクトリ名
	uint8_t *cache_buf;		//キャッシュデータ
	int32_t cache_size;		//キャッシュデータのサイズ
	uint8_t is_loaded;		//テーマが読み込まれたか
};

enum
{
	_RET_ERR = -1,
	_RET_OK = 0,
	_RET_UNFOUND = 1,
	_RET_UNMATCH_SIZE = 2,
	_RET_NOTEXIST_FILE = 3
};

//---------------------


//==============================
// テーマ読み込み
//==============================


/** _theme_diritem 破棄ハンドラ */

static void _diritem_destroy(mList *list,mListItem *item)
{
	mFree(_DIRITEM(item)->path);
}

/** _theme_item 破棄ハンドラ */

static void _themeitem_destroy(mList *list,mListItem *item)
{
	_theme_item *p = _THEMEITEM(item);

	mListDeleteAll(&p->dirlist);
	
	mFree(p->path);
	mFree(p->cache_buf);
}

/** テーマアイテム追加
 *
 * パスをセットして、キャッシュデータを読み込み。
 * キャッシュが存在しない場合も含める。
 *
 * parent: 親テーマ。NULL でルート
 * return: NULL で失敗 */

static _theme_item *_add_theme_item(mList *list,_theme_item *parent,const char *path)
{
	_theme_item *pi,*ins;
	mStr str = MSTR_INIT;

	//挿入位置
	//(親の直後、または親の子の最後)

	if(!parent)
		ins = NULL;
	else
	{
		ins = _THEMEITEM(parent->i.next);

		for(; ins && ins->parent == parent; ins = _THEMEITEM(ins->i.next));
	}

	//追加

	pi = (_theme_item *)mListInsertNew(list, (mListItem *)ins, sizeof(_theme_item));

	if(pi)
	{
		pi->dirlist.item_destroy = _diritem_destroy;
		pi->parent = parent;

		//パス

		pi->path = mStrdup(path);

		//キャッシュデータ

		mStrSetText(&str, path);
		mStrPathJoin(&str, "icon-theme.cache");

		if(mReadFileFull_alloc(str.buf, 0, &pi->cache_buf, &pi->cache_size) == MLKERR_OK)
		{
			//major バージョン (uint16) が 1 以外の場合
			
			if(pi->cache_size < 12
				|| pi->cache_buf[0] != 0 || pi->cache_buf[1] != 1)
			{
				mFree(pi->cache_buf);
				pi->cache_buf = NULL;
			}
		}

		mStrFree(&str);
	}

	return pi;
}

/** テーマ名からテーマアイテム追加 */

static void _add_theme_item_name(mList *list,_theme_item *parent,const char *name)
{
	mStr str = MSTR_INIT;
	const char *paths[] = {"/usr/share/icons", "/usr/local/share/icons"};
	int i;

	//"~/.icons"

	mStrPathSetHome_join(&str, ".icons");
	mStrPathJoin(&str, name);

	if(mIsExistDir(str.buf))
		_add_theme_item(list, parent, str.buf);

	//"~/.local/share/icons"

	mStrPathSetHome_join(&str, ".local/share/icons");
	mStrPathJoin(&str, name);

	if(mIsExistDir(str.buf))
		_add_theme_item(list, parent, str.buf);

	//絶対パス

	for(i = 0; i < 2; i++)
	{
		mStrSetText(&str, paths[i]);
		mStrPathJoin(&str, name);

		if(mIsExistDir(str.buf))
			_add_theme_item(list, parent, str.buf);
	}

	mStrFree(&str);
}

/** テーマ読み込み
 *
 * [!] アイコンテーマではない場合は、アイテムを削除する */

static void _load_theme(mList *list,_theme_item *pi_theme)
{
	mIniRead *ini;
	_theme_diritem *pi_dir;
	const char *pc_dir,*pcend,*pc_inh;
	char *name;

	//index.theme 読み込み
	//(index.theme がない場合、アイコンテーマではない)

	if(mIniRead_loadFile_join(&ini, pi_theme->path, "index.theme"))
		goto ERR;

	//グループセット

	if(!mIniRead_setGroup(ini, "icon theme"))
		goto ERR;

	//サブディレクトリリスト (項目がない場合、アイコンテーマではない)

	pc_dir = mIniRead_getText(ini, "directories", NULL);
	if(!pc_dir) goto ERR;

	//ロード済みにセット

	pi_theme->is_loaded = 1;

	//Inherits
	//子として継承するテーマを追加。
	//追加したテーマは後で読み込む。

	pc_inh = mIniRead_getText(ini, "inherits", NULL);

	if(pc_inh)
	{
		while(mStringGetNextSplit(&pc_inh, &pcend, ','))
		{
			name = mStrndup(pc_inh, pcend - pc_inh);

			if(name)
			{
				_add_theme_item_name(list, pi_theme, name);
				mFree(name);
			}
		
			pc_inh = pcend;
		}
	}

	//サブディレクトリのリスト追加

	while(mStringGetNextSplit(&pc_dir, &pcend, ','))
	{
		pi_dir = (_theme_diritem *)mListAppendNew(&pi_theme->dirlist, sizeof(_theme_diritem));

		if(pi_dir)
		{
			//相対パス
			
			pi_dir->path = mStrndup(pc_dir, pcend - pc_dir);

			//画像サイズ取得 (type が scalable 以外の場合)

			if(mIniRead_setGroup(ini, pi_dir->path))
			{
				if(!mIniRead_compareText(ini, "type", "scalable", TRUE))
					pi_dir->size = mIniRead_getInt(ini, "size", 0);
			}

			//type=scalable、サイズが指定されていない、
			//scale が2以上の場合は削除

			if(pi_dir->size == 0 || mIniRead_getInt(ini, "scale", 0) > 1)
				mListDelete(&pi_theme->dirlist, MLISTITEM(pi_dir));
			else
				pi_dir->hash = mCalcStringHash(pi_dir->path);
		}
	
		pc_dir = pcend;
	}

	mIniRead_end(ini);

	return;

	//エラー時は削除
ERR:
	mIniRead_end(ini);
	mListDelete(list, MLISTITEM(pi_theme));
}

/** 未読込のテーマを読み込み */

static void _load_theme_unloaded(mList *list)
{
	_theme_item *pi,*parent,*next;
	int flag;

	//親から順に読み込み

	while(1)
	{
		//読み込まれていないテーマを検索 (親を取得)

		flag = 1;

		for(pi = _THEMEITEM(list->top); pi; pi = _THEMEITEM(pi->i.next))
		{
			if(!pi->is_loaded)
			{
				parent = pi->parent;
				flag = 0;
				break;
			}
		}

		if(flag) break;
	
		//指定テーマを親に持つテーマを読み込み
	
		for(pi = _THEMEITEM(list->top); pi; pi = next)
		{
			next = _THEMEITEM(pi->i.next);

			if(pi->parent == parent && !pi->is_loaded)
			{
				//さらに子がある場合、追加される
				//[!] 失敗した場合は削除される
				
				_load_theme(list, pi);
			}
		}
	}
}

/** ルートテーマと継承テーマを読み込み */

static void _load_root_theme(mList *list,const char *theme_name)
{
	char *pc;
	int len,add;

	//ルートテーマ以下を読み込み

	if(theme_name)
	{
		_add_theme_item_name(list, NULL, theme_name);
		_load_theme_unloaded(list);
	}

	//終端に hicolor がない場合、追加

	if(!list->bottom)
		add = TRUE;
	else
	{
		pc = _THEMEITEM(list->bottom)->path;
		len = strlen(pc);
		add = (len > 7 && strcmp(pc + len - 7, "hicolor") != 0);
	}

	if(add)
	{
		_add_theme_item_name(list, NULL, "hicolor");
		_load_theme_unloaded(list);
	}

	//debug

#if 0
	_theme_item *pi;
	for(pi = _THEMEITEM(list->top); pi; pi = _THEMEITEM(pi->i.next))
		mDebug("- %s\n", pi->path);
#endif
}


//=================================
// アイコン検索
//=================================


/** ハッシュ値計算 */

static uint32_t _calc_hash(const char *pc)
{
	uint32_t h = *pc;

	for(pc++; *pc; pc++)
		h = (h << 5) - h + *pc;

	return h;
}

/** キャッシュバッファから指定位置のポインタ取得 */

static int _get_cacheptr(_theme_item *pi,char **dst,uint32_t pos)
{
	if(pos >= pi->cache_size)
		return 1;
	else
	{
		*dst = (char *)(pi->cache_buf + pos);
		return 0;
	}
}

/** キャッシュバッファから 16bit 値読み込み */

static int _get_cache16(_theme_item *pi,uint16_t *dst,uint32_t pos)
{
	uint8_t *ps;

	if(pos + 2 > pi->cache_size)
		return 1;
	else
	{
		ps = pi->cache_buf + pos;
		*dst = ((uint16_t)ps[0] << 8) | ps[1];
		return 0;
	}
}

/** キャッシュバッファから 32bit 値読み込み */

static int _get_cache32(_theme_item *pi,uint32_t *dst,uint32_t pos)
{
	uint8_t *ps;

	if(pos + 4 > pi->cache_size)
		return 1;
	else
	{
		ps = pi->cache_buf + pos;
		*dst = ((uint32_t)ps[0] << 24) | (ps[1] << 16) | (ps[2] << 8) | ps[3];
		return 0;
	}
}

/** ディレクトリから直接ファイルを検索 */

static int _search_icon_dir(_theme_item *theme,const char *icon_name,int icon_size,mStr *dstpath)
{
	_theme_diritem *pi;

	MLK_LIST_FOR(theme->dirlist, pi, _theme_diritem)
	{
		if(pi->size == icon_size)
		{
			mStrSetText(dstpath, theme->path);
			mStrPathJoin(dstpath, pi->path);
			mStrPathJoin(dstpath, icon_name);
			mStrAppendText(dstpath, ".png");

			if(mIsExistFile(dstpath->buf))
				return _RET_OK;
		}
	}

	return _RET_UNFOUND;
}

/** アイコン名からファイルパス取得 (指定テーマ内のみ)
 *
 * dstpath: 拡張子は png で固定 */

static int _get_iconfile_path(_theme_item *theme,const char *icon_name,int icon_size,mStr *dstpath)
{
	uint32_t hash,offset_dirlist,offset,offset2,num,i;
	uint16_t dirindex;
	char *pc;
	_theme_diritem *pi;

	//アイコンキャッシュがない場合、直接検索

	if(!theme->cache_buf)
		return _search_icon_dir(theme, icon_name, icon_size, dstpath);

	//ハッシュ値からデータ位置取得

	hash = _calc_hash(icon_name);

	if(_get_cache32(theme, &offset, 4)		//ハッシュデータのオフセット位置
		|| _get_cache32(theme, &num, offset)	//ハッシュテーブル数
		|| _get_cache32(theme, &offset, offset + 4 + (hash % num) * 4)) //指定ハッシュのデータ位置
		return _RET_ERR;

	//アイコン名検索

	while(1)
	{
		if(offset == 0 || offset == 0xffffffff)
			return _RET_UNFOUND;
	
		//名前を比較

		if(_get_cache32(theme, &offset2, offset + 4)	//名前のオフセット位置
			|| _get_cacheptr(theme, &pc, offset2))		//文字列の位置
			return _RET_ERR;

		if(strcmp(pc, icon_name) == 0) break;

		//次のデータ位置 (0xffffffff で終了)

		if(_get_cache32(theme, &offset, offset))
			return _RET_ERR;
	}

	//名前が一致した場合、アイコンがあるサブディレクトリのリストを取得

	if(_get_cache32(theme, &offset_dirlist, 8)	//ディレクトリリストのオフセット位置
		|| _get_cache32(theme, &offset, offset + 8)	//ディレクトリデータのオフセット位置
		|| _get_cache32(theme, &num, offset))		//ディレクトリの数
		return _RET_ERR;

	offset += 4;

	for(i = 0; i < num; i++, offset += 8)
	{
		//パス名取得
		
		if(_get_cache16(theme, &dirindex, offset)	//インデックス位置
			|| _get_cache32(theme, &offset2, offset_dirlist + 4 + (dirindex << 2)) //ディレクトリ文字列のオフセット位置
			|| _get_cacheptr(theme, &pc, offset2))	//パス名
			return _RET_ERR;

		//サブディレクトリリストから検索し、アイコンサイズを確認

		hash = mCalcStringHash(pc);

		for(pi = _DIRITEM(theme->dirlist.top); pi; pi = _DIRITEM(pi->i.next))
		{
			if(hash == pi->hash && strcmp(pc, pi->path) == 0)
			{
				if(pi->size == icon_size)
				{
					mStrSetText(dstpath, theme->path);
					mStrPathJoin(dstpath, pc);
					mStrPathJoin(dstpath, icon_name);
					mStrAppendText(dstpath, ".png");

					//ファイルが存在するか

					if(mIsExistFile(dstpath->buf))
						return _RET_OK;
					else
						return _RET_NOTEXIST_FILE;
				}

				//サイズが一致しない場合、次のディレクトリへ
				break;
			}
		}
	}

	//アイコンは見つかったが、一致するサイズがない
	return _RET_UNMATCH_SIZE;
}


//==============================
// main
//==============================


/**@ アイコンテーマ解放 */

void mIconTheme_free(mIconTheme p)
{
	if(p)
	{
		mListDeleteAll((mList *)p);
		mFree(p);
	}
}

/**@ アイコンテーマ読み込み
 *
 * @d:"hicolor" は、常に終端のテーマとして設定される。
 *
 * @p:name テーマ名。\
 *  NULL で、"hicolor"。\
 *  GUI スタイルのテーマ名は、mGuiGetIconTheme() で取得できる。
 * @r:0 で失敗 */

mIconTheme mIconTheme_loadTheme(const char *name)
{
	mList *list;

	//リスト

	list = (mList *)mMalloc0(sizeof(mList));
	if(!list) return 0;

	list->item_destroy = _themeitem_destroy;

	//読み込み

	_load_root_theme(list, name);

	//

	if(list->top)
		return (mIconTheme)list;
	else
	{
		mFree(list);
		return 0;
	}
}

/**@ 指定アイコンのファイルパスを取得 (PNG)
 *
 * @p:name アイコン名
 * @p:size 画像サイズ */

mlkbool mIconTheme_getIconPath(mIconTheme p,mStr *str,const char *name,int size)
{
	mList *list = (mList *)p;
	_theme_item *pi;
	int ret;

	//ルートテーマから順に検索

	for(pi = _THEMEITEM(list->top); pi; pi = _THEMEITEM(pi->i.next))
	{
		ret = _get_iconfile_path(pi, name, size, str);
		if(ret == _RET_OK)
			return TRUE;
	}

	//全てのテーマで見つからなかった

	mStrFree(str);

	return FALSE;
}

/**@ 指定アイコンを mImageBuf で読み込み
 *
 * @d:読み込み後、サイズが一致しない場合は NULL が返る。 */

mImageBuf *mIconTheme_getIcon_imagebuf(mIconTheme p,const char *name,int size)
{
	mStr str = MSTR_INIT;
	mLoadImageOpen open;
	mLoadImageType tp;
	mImageBuf *img;

	//ファイルパス取得

	if(!mIconTheme_getIconPath(p, &str, name, size))
		return NULL;

	//読み込み

	open.type = MLOADIMAGE_OPEN_FILENAME;
	open.filename = str.buf;

	mLoadImage_checkPNG(&tp, 0, 0);

	img = mImageBuf_loadImage(&open, &tp, 32, 0);

	mStrFree(&str);

	//サイズ確認

	if(img->width != size || img->height != size)
	{
		mImageBuf_free(img);
		return NULL;
	}

	return img;
}

