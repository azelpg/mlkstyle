/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * mColorPrev
 *****************************************/

#include "mlk_gui.h"
#include "mlk_widget_def.h"
#include "mlk_widget.h"
#include "mlk_colorprev.h"
#include "mlk_pixbuf.h"
#include "mlk_guicol.h"


/**@ データ削除 */

void mColorPrevDestroy(mWidget *p)
{

}

/**@ 作成 */

mColorPrev *mColorPrevNew(mWidget *parent,int size,uint32_t fstyle)
{
	mColorPrev *p;
	
	if(size < sizeof(mColorPrev))
		size = sizeof(mColorPrev);
	
	p = (mColorPrev *)mWidgetNew(parent, size);
	if(!p) return NULL;
	
	p->wg.draw = mColorPrevHandle_draw;
	
	p->cp.fstyle = fstyle;
	p->cp.rgbcol = 0xffffff;
	
	return p;
}

/**@ 作成
 *
 * @p:w,h 推奨サイズにセットされる */

mColorPrev *mColorPrevCreate(mWidget *parent,
	int w,int h,uint32_t margin_pack,uint32_t fstyle,mRgbCol col)
{
	mColorPrev *p;

	p = mColorPrevNew(parent, 0, fstyle);
	if(!p) return NULL;

	p->wg.hintW = w;
	p->wg.hintH = h;

	mWidgetSetMargin_pack4(MLK_WIDGET(p), margin_pack);

	p->cp.rgbcol = col;

	return p;
}

/**@ RGB 色セット */

void mColorPrevSetColor(mColorPrev *p,mRgbCol col)
{
	col &= 0xffffff;

	if(col != p->cp.rgbcol)
	{
		p->cp.rgbcol = col;

		mWidgetRedraw(MLK_WIDGET(p));
	}
}

/**@ draw ハンドラ関数 */

void mColorPrevHandle_draw(mWidget *wg,mPixbuf *pixbuf)
{
	mColorPrev *p = MLK_COLORPREV(wg);
	mPixCol col;
	int w,h;

	w = wg->w;
	h = wg->h;
	col = mRGBtoPix(p->cp.rgbcol);

	if(p->cp.fstyle & MCOLORPREV_S_FRAME)
	{
		//枠付き
		
		mPixbufBox(pixbuf, 0, 0, w, h, MGUICOL_PIX(FRAME));
		mPixbufDraw3DFrame(pixbuf, 1, 1, w - 2, h - 2, mRGBtoPix(0x404040), MGUICOL_PIX(WHITE));
		mPixbufFillBox(pixbuf, 2, 2, w - 4, h - 4, col);
	}
	else
		mPixbufFillBox(pixbuf, 0, 0, w, h, col);
}

