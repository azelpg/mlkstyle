/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * mFontButton
 *****************************************/

#include "mlk_gui.h"
#include "mlk_widget_def.h"
#include "mlk_widget.h"
#include "mlk_fontbutton.h"
#include "mlk_str.h"

#include "mlk_pv_widget.h"



/* ボタンテキストセット */

static void _set_button_text(mFontButton *p)
{
	mStr str = MSTR_INIT;

	mFontInfoGetText_family_size(&str, &p->fbt.info);
	
	mButtonSetText(MLK_BUTTON(p), str.buf);

	mStrFree(&str);

	mButtonHandle_calcHint(MLK_WIDGET(p));
}

/* ボタン押し時のハンドラ */

static void _pressed_handle(mWidget *wg)
{
	mFontButton *p = MLK_FONTBUTTON(wg);

	//フォント選択ダイアログ

	if(mSysDlg_selectFont(wg->toplevel, p->fbt.flags, &p->fbt.info))
	{
		//ボタンテキスト

		_set_button_text(p);
	
		//通知
	
		mWidgetEventAdd_notify(wg, NULL, MFONTBUTTON_N_CHANGE_FONT, 0, 0);
	}
}


//==========================
// main
//==========================


/**@ mFontButton データ解放 */

void mFontButtonDestroy(mWidget *wg)
{
	mFontInfoFree(&MLK_FONTBUTTON(wg)->fbt.info);

	mButtonDestroy(wg);
}

/**@ 作成 */

mFontButton *mFontButtonNew(mWidget *parent,int size,uint32_t fstyle)
{
	mFontButton *p;
	
	if(size < sizeof(mFontButton))
		size = sizeof(mFontButton);
	
	p = (mFontButton *)mButtonNew(parent, size, MBUTTON_S_COPYTEXT);
	if(!p) return NULL;

	p->wg.destroy = mFontButtonDestroy;
	
	p->btt.pressed = _pressed_handle;

	p->fbt.flags = (fstyle & MFONTBUTTON_S_ALL_INFO)?
		MSYSDLG_FONT_F_ALL: MSYSDLG_FONT_F_NORMAL;

	return p;
}

/**@ 作成 */

mFontButton *mFontButtonCreate(mWidget *parent,int id,uint32_t flayout,uint32_t margin_pack,uint32_t fstyle)
{
	mFontButton *p;

	p = mFontButtonNew(parent, 0, fstyle);
	if(p)
		__mWidgetCreateInit(MLK_WIDGET(p), id, flayout, margin_pack);

	return p;
}

/**@ フォントダイアログのフラグをセット
 *
 * @d:デフォルトで、拡張情報以外あり */

void mFontButtonSetFlags(mFontButton *p,uint32_t flags)
{
	p->fbt.flags = flags;
}

/**@ テキストからフォント情報をセット */

void mFontButtonSetInfo_text(mFontButton *p,const char *text)
{
	mFontInfoSetFromText(&p->fbt.info, text);

	_set_button_text(p);
}

/**@ フォント情報を文字列フォーマットで取得 */

void mFontButtonGetInfo_text(mFontButton *p,mStr *str)
{
	mFontInfoToText(str, &p->fbt.info);
}

/**@ フォント情報をセット */

void mFontButtonSetInfo(mFontButton *p,const mFontInfo *src)
{
	mFontInfoCopy(&p->fbt.info, src);

	_set_button_text(p);
}

/**@ フォント情報を取得 */

void mFontButtonGetInfo(mFontButton *p,mFontInfo *dst)
{
	mFontInfoCopy(dst, &p->fbt.info);
}


