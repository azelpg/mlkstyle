/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_PV_PIXBUF_H
#define MLK_PV_PIXBUF_H

#define MPIXBUFBASE(p)  ((mPixbufBase *)(p))

/** mPixbuf 内部データ */

typedef struct
{
	mFuncPixbufSetBuf setbuf;	//バッファに色をセットする関数
	mRect rcclip,		//現在のクリッピング範囲
		rcclip_root;	//システムで指定するクリッピング範囲 (rcclip は常にこの範囲内となる)
	int offsetX,		//描画時のオフセット位置
		offsetY;
	uint8_t fclip,		//クリッピングフラグ
		fcreate;		//作成時のフラグ
}mPixbufPrivate;

/** mPixbuf 基本データ */

typedef struct
{
	mPixbuf b;
	mPixbufPrivate pv;
}mPixbufBase;


enum
{
	MPIXBUF_CLIPF_HAVE = 1<<0,		//クリッピング範囲あり (イメージ範囲外も含む)
	MPIXBUF_CLIPF_HAVE_ROOT = 1<<1,	//root 範囲あり (イメージ範囲内)
	MPIXBUF_CLIPF_OUT = 1<<2		//rcclip の範囲がイメージ外 (描画可能な範囲がない)
};


mlkbool __mPixbufSetClipRoot(mPixbuf *p,mRect *rc);

#endif
