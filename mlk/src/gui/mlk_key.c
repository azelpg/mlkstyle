/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * キー
 *****************************************/

#include "mlk_gui.h"
#include "mlk_key.h"

#include "mlk_pv_gui.h"


/**@ キー識別子から、矢印キーの移動方向を取得
 *
 * @r:-1 で矢印キーではない。\
 * bit0: 0=左右、1=上下。\
 * bit1: 0=左/上、1=右/下。\
 * 0=左、1=上、2=右、3=下。 */

int mKeyGetArrowDir(uint32_t key)
{
	switch(key)
	{
		case MKEY_LEFT:
		case MKEY_KP_LEFT:
			return 0;
		case MKEY_UP:
		case MKEY_KP_UP:
			return 1;
		case MKEY_RIGHT:
		case MKEY_KP_RIGHT:
			return 2;
		case MKEY_DOWN:
		case MKEY_KP_DOWN:
			return 3;
	}

	return -1;
}

/**@ キー識別子が矢印キーかどうか */

mlkbool mKeyIsArrow(uint32_t key)
{
	return (key == MKEY_LEFT || key == MKEY_KP_LEFT
		|| key == MKEY_RIGHT || key == MKEY_KP_RIGHT
		|| key == MKEY_UP || key == MKEY_KP_UP
		|| key == MKEY_DOWN || key == MKEY_KP_DOWN);
}

/**@ キー識別子がアルファベットかどうか */

mlkbool mKeyIsAlphabet(uint32_t key)
{
	return ((key >= 'A' && key <= 'Z') || (key >= 'a' && key <= 'z'));
}

/**@ バックエンドのキーコードから、キー識別子 (MKEY_*) を取得
 *
 * @r:変換できなかった場合、0 */

uint32_t mKeycodeToKey(uint32_t code)
{
	return (MLKAPP->bkend.keycode_to_code)(code);
}

/**@ キー識別子から、キーの名前の文字列を取得
 *
 * @r:成功したか */

mlkbool mKeycodeGetName(mStr *str,uint32_t code)
{
	return (MLKAPP->bkend.keycode_getname)(str, code);
}

