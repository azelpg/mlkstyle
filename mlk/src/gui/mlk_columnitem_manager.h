/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_COLUMNITEM_MANAGER_H
#define MLK_COLUMNITEM_MANAGER_H

#include "mlk_columnitem.h"

enum
{
	MCIMANAGER_ONCLICK_CHANGE_FOCUS = 1<<0,
	MCIMANAGER_ONCLICK_ON_FOCUS = 1<<1,
	MCIMANAGER_ONCLICK_KEY_SEL = 1<<2
};

#ifdef __cplusplus
extern "C" {
#endif

void __mColumnItemSetText(mColumnItem *pi,const char *text,mlkbool copy,mlkbool multi_column,mFont *font);

/* mCIManager */

void mCIManagerInit(mCIManager *p,mlkbool multi_sel);
void mCIManagerFree(mCIManager *p);

mColumnItem *mCIManagerInsertItem(mCIManager *p,
	mColumnItem *ins,int size,
	const char *text,int icon,uint32_t flags,intptr_t param,mFont *font,mlkbool multi_column);

void mCIManagerDeleteAllItem(mCIManager *p);
mlkbool mCIManagerDeleteItem(mCIManager *p,mColumnItem *item);
mlkbool mCIManagerDeleteItem_atIndex(mCIManager *p,int index);

void mCIManagerSelectAll(mCIManager *p);
void mCIManagerUnselectAll(mCIManager *p);

int mCIManagerGetItemIndex(mCIManager *p,mColumnItem *item);
mColumnItem *mCIManagerGetItem_atIndex(mCIManager *p,int index);
mColumnItem *mCIManagerGetItem_fromParam(mCIManager *p,intptr_t param);
mColumnItem *mCIManagerGetItem_fromText(mCIManager *p,const char *text);

mlkbool mCIManagerSetFocusItem(mCIManager *p,mColumnItem *item);
mlkbool mCIManagerSetFocusItem_atIndex(mCIManager *p,int index);
mlkbool mCIManagerSetFocus_toHomeEnd(mCIManager *p,mlkbool home);

mlkbool mCIManagerMoveItem_updown(mCIManager *p,mColumnItem *item,mlkbool down);
mlkbool mCIManagerFocusItem_updown(mCIManager *p,mlkbool down);
int mCIManagerOnClick(mCIManager *p,uint32_t key_state,mColumnItem *item);

int mCIManagerGetMaxWidth(mCIManager *p);

#ifdef __cplusplus
}
#endif

#endif
