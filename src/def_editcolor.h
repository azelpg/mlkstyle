/*$
mlk style editor
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * 色編集用の定義
 **********************************/


/* メニューTRID */

enum
{
	TRID_COLMENU_TOP_EDIT = 1,
	TRID_COLMENU_TOP_TEST,

	TRID_COLMENU_EDIT_RESET = 1000,
	TRID_COLMENU_EDIT_BLACK,

	TRID_COLMENU_TEST_ITEM1 = 1100,
	TRID_COLMENU_TEST_ITEM2
};

/* メニューデータ */

static const uint16_t g_colmenu_dat[] = {
TRID_COLMENU_TOP_EDIT,
MMENUBAR_ARRAY16_SUB_START,
	TRID_COLMENU_EDIT_RESET, MMENUBAR_ARRAY16_SEP, TRID_COLMENU_EDIT_BLACK,
MMENUBAR_ARRAY16_SUB_END,

TRID_COLMENU_TOP_TEST,
MMENUBAR_ARRAY16_SUB_START,
	TRID_COLMENU_TEST_ITEM1, MMENUBAR_ARRAY16_SEP, TRID_COLMENU_TEST_ITEM2,
MMENUBAR_ARRAY16_SUB_END,

MMENUBAR_ARRAY16_END
};

/* 色名 */

static const char *g_color_name[] = {
"FACE",
"FACE_DISABLE",
"FACE_SELECT",
"FACE_SELECT_UNFOCUS",
"FACE_SELECT_LIGHT",
"FACE_TEXTBOX",
"FACE_DARK",
"FRAME",
"FRAME_BOX",
"FRAME_FOCUS",
"FRAME_DISABLE",
"GRID",
"TEXT",
"TEXT_DISABLE",
"TEXT_SELECT",
"BUTTON_FACE",
"BUTTON_FACE_PRESS",
"BUTTON_FACE_DEFAULT",
"SCROLLBAR_FACE",
"SCROLLBAR_GRIP",
"MENU_FACE",
"MENU_FRAME",
"MENU_SEP",
"MENU_SELECT",
"MENU_TEXT",
"MENU_TEXT_DISABLE",
"MENU_TEXT_SHORTCUT",
"TOOLTIP_FACE",
"TOOLTIP_FRAME",
"TOOLTIP_TEXT"
};

//-----------------

/* アイコンボタンイメージリスト */

static const unsigned char g_png_iconbtt[226] = {
0x89,0x50,0x4e,0x47,0x0d,0x0a,0x1a,0x0a,0x00,0x00,0x00,0x0d,0x49,0x48,0x44,0x52,
0x00,0x00,0x00,0x60,0x00,0x00,0x00,0x10,0x02,0x03,0x00,0x00,0x00,0x94,0xd4,0x92,
0x6a,0x00,0x00,0x00,0x09,0x50,0x4c,0x54,0x45,0x00,0xff,0x00,0xff,0xff,0xff,0x00,
0x00,0x00,0x55,0x8d,0x85,0x97,0x00,0x00,0x00,0x94,0x49,0x44,0x41,0x54,0x78,0x5e,
0x75,0xd0,0xbb,0x11,0x83,0x30,0x0c,0x80,0xe1,0x5f,0x45,0x32,0x01,0x69,0x3c,0x8d,
0x46,0xa0,0xb0,0x1b,0x36,0x90,0xa7,0xa1,0xc8,0x08,0x72,0x21,0x4d,0x99,0x02,0xc8,
0x85,0x70,0xa8,0xd4,0x77,0x7a,0xc2,0x6d,0xc8,0x0c,0x4d,0xe1,0xa1,0xd0,0x66,0x68,
0x15,0x5e,0x0d,0x78,0x3a,0x92,0x2b,0x94,0x15,0x49,0x47,0x32,0xa0,0xa7,0xee,0xe0,
0xd0,0xb7,0xe4,0x19,0x46,0x20,0x11,0xd0,0xbb,0xca,0xa8,0x60,0x0d,0x28,0x86,0x0c,
0x43,0xdc,0xa0,0x2f,0x2a,0x63,0x06,0xab,0x3b,0xe4,0x40,0x7c,0xda,0x20,0x1d,0x7a,
0x1c,0x10,0x88,0xf3,0x0f,0xb1,0xcd,0x38,0x2a,0xbe,0xad,0x7a,0xaa,0xf8,0xcf,0x0c,
0x07,0x0c,0xc0,0x96,0x3b,0x28,0x7a,0x5d,0xf7,0x80,0x8c,0xf3,0x81,0x06,0x48,0x94,
0x55,0xf2,0x7d,0x7e,0x89,0x01,0xd4,0x87,0x5e,0x9e,0x38,0x71,0x1f,0x1f,0xfa,0xda,
0x4d,0x15,0xb3,0xbb,0x68,0x76,0x00,0x00,0x00,0x00,0x49,0x45,0x4e,0x44,0xae,0x42,
0x60,0x82 };

/* コピーボタン */

static const unsigned char g_img_copy[]={
0xff,0xff,0x7f,0x55,0x03,0x00,0x70,0x55,0xf3,0xff,0xf3,0xff,0x73,0x55,0x33,0xc0,
0x73,0x55,0xf3,0xcf,0x73,0x55,0x73,0xcd,0x73,0x55,0x73,0xcd,0x73,0x55,0x73,0xcd,
0x73,0x55,0x73,0xcd,0x73,0x55,0x73,0xcd,0x73,0x55,0x73,0xcd,0xf3,0xff,0x73,0xcd,
0x03,0x00,0x70,0xcd,0xff,0xcf,0xff,0xcf,0x55,0x0d,0x00,0xc0,0x55,0xfd,0xff,0xff };

/* 貼り付けボタン */

static const unsigned char g_img_paste[]={
0x55,0xff,0x7f,0x55,0xf5,0x00,0xc0,0x57,0x0d,0xfc,0x0f,0x5c,0x03,0x00,0x00,0x70,
0x03,0x00,0x00,0xf0,0x03,0x00,0x00,0xc0,0x03,0x00,0x00,0xc0,0x03,0x00,0xfc,0xcf,
0x03,0x00,0xfc,0xcf,0x03,0x00,0xfc,0xcf,0x03,0x00,0xfc,0xcf,0x03,0x00,0xfc,0xcf,
0x03,0x00,0xfc,0xcf,0xfd,0xff,0xfc,0xcf,0x55,0xd5,0x00,0xc0,0x55,0xd5,0xff,0xff };

/* アンドゥボタン */

static const unsigned char g_img_undo[]={
0x55,0x55,0x55,0x55,0x55,0x55,0x55,0x55,0x55,0x55,0x55,0x55,0x55,0x55,0xff,0x57,
0x5d,0xd5,0x00,0x5c,0x73,0x35,0x00,0x70,0xc3,0x0d,0xfc,0xc0,0x03,0x03,0x57,0xc3,
0x03,0xc0,0x55,0xc3,0x03,0x70,0x55,0xc3,0x03,0xc0,0xd5,0x70,0x03,0x00,0xd7,0x70,
0x03,0x00,0x5c,0x5f,0xfd,0xff,0x57,0x55,0x55,0x55,0x55,0x55,0x55,0x55,0x55,0x55 };

