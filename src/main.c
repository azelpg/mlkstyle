/*$
mlk style editor
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * mlkstyle - main
 **********************************/

#include "mlk_gui.h"
#include "mlk_widget_def.h"
#include "mlk_widget.h"
#include "mlk_window.h"
#include "mlk_sysdlg.h"
#include "mlk_event.h"
#include "mlk_fontinfo.h"
#include "mlk_menubar.h"
#include "mlk_label.h"
#include "mlk_button.h"
#include "mlk_lineedit.h"
#include "mlk_fontbutton.h"
#include "mlk_str.h"
#include "mlk_guistyle.h"

#include "def.h"
#include "deftrans.h"


//-----------------

#define VERSION_TEXT  "mlk style editor ver 1.0.5\n\nCopyright (c) 2020-2022 Azel"

typedef struct
{
	MLK_TOPLEVEL_DEF

	mFontButton *fontbtt;
	mLineEdit *le_icontheme;

	mStr strOpenDir,
		strSaveDir;

	mGuiStyleInfo info;
}MainWindow;

/* メインウィンドウ */
enum
{
	TRID_MAINWIN_OPEN,
	TRID_MAINWIN_SAVE,
	TRID_MAINWIN_EXIT,
	TRID_MAINWIN_FONT,
	TRID_MAINWIN_ICONTHEME,
	TRID_MAINWIN_SELECT,
	TRID_MAINWIN_COLOR,
	TRID_MAINWIN_EDIT,
	
	TRID_MAINWIN_HELP_FONT = 1000,
	TRID_MAINWIN_HELP_ABOUT
};

enum
{
	WID_BTT_OPEN = 100,
	WID_BTT_SAVE,
	WID_BTT_EXIT,
	WID_FONTBTT,
	WID_FONT_HELP,
	WID_ICONTHEME_SELECT,
	WID_COLOR_EDIT
};

/* メインメニュー */
enum
{
	TRID_MENU_TOP_HELP = 1,

	TRID_MENU_HELP_HELP = 1000,
	TRID_MENU_HELP_ABOUT
};

/* メインメニューデータ */
static const uint16_t g_mainmenu_dat[] = {
TRID_MENU_TOP_HELP,
MMENUBAR_ARRAY16_SUB_START,
	TRID_MENU_HELP_HELP, TRID_MENU_HELP_ABOUT,
MMENUBAR_ARRAY16_SUB_END,

MMENUBAR_ARRAY16_END
};

//-----------------

void ColorEditDlg_run(mWindow *parent,mGuiStyleInfo *info);
mlkbool SelIconThemeDlg_run(mWindow *parent,mStr *strdst);

//-----------------




/* 設定ファイルを開く */

static void _open_file(MainWindow *p,const char *filename)
{
	mGuiStyle_load(filename, &p->info);

	//セット

	mFontButtonSetInfo(p->fontbtt, &p->info.fontinfo);

	mLineEditSetText(p->le_icontheme, p->info.strIcontheme.buf);

	mGuiStyle_setColor(&p->info);

	mWidgetRedraw(MLK_WIDGET(p));
}

/* 開く */

static void _cmd_open_file(MainWindow *p)
{
	mStr str = MSTR_INIT;

	if(!mSysDlg_openFile(MLK_WINDOW(p), "*.conf\t*.conf\tAll files\t*", 0, p->strOpenDir.buf, 0, &str))
		return;

	mStrPathGetDir(&p->strOpenDir, str.buf);

	_open_file(p, str.buf);

	mStrFree(&str);
}

/* 保存 */

static void _cmd_save_file(MainWindow *p)
{
	mStr str = MSTR_INIT;

	//値取得

	mLineEditGetTextStr(p->le_icontheme, &p->info.strIcontheme);

	//

	mStrSetText(&str, "az-mlk-style.conf");

	if(mSysDlg_saveFile(MLK_WINDOW(p), "*.conf\tconf\tAll files\t*", 0, p->strSaveDir.buf,
		MSYSDLG_FILE_F_DEFAULT_FILENAME, &str, NULL))
	{
		mStrPathGetDir(&p->strSaveDir, str.buf);

		mGuiStyle_save(str.buf, &p->info);
	}

	mStrFree(&str);
}


//===========================
// ハンドラ
//===========================


/* コマンド */

static void _event_command(MainWindow *p,int id)
{
	switch(id)
	{
		//ヘルプ
		case TRID_MENU_HELP_HELP:
			mMessageBoxOK(MLK_WINDOW(p), MLK_TR2(TRGROUP_ID_MAINWIN, TRID_MAINWIN_HELP_ABOUT));
			break;
		//バージョン情報
		case TRID_MENU_HELP_ABOUT:
			mSysDlg_about(MLK_WINDOW(p), VERSION_TEXT);
			break;
	}
}

/* 通知 */

static void _event_notify(MainWindow *p,mEventNotify *ev)
{
	int type = ev->notify_type;

	switch(ev->id)
	{
		//フォントボタン
		case WID_FONTBTT:
			if(type == MFONTBUTTON_N_CHANGE_FONT)
				mFontButtonGetInfo(p->fontbtt, &p->info.fontinfo);
			break;
		//フォントヘルプ
		case WID_FONT_HELP:
			mMessageBoxOK(MLK_WINDOW(p), MLK_TR2(TRGROUP_ID_MAINWIN, TRID_MAINWIN_HELP_FONT));
			break;
		//テーマ選択
		case WID_ICONTHEME_SELECT:
			if(SelIconThemeDlg_run(MLK_WINDOW(p), &p->info.strIcontheme))
				mLineEditSetText(p->le_icontheme, p->info.strIcontheme.buf);
			break;
		//色編集
		case WID_COLOR_EDIT:
			ColorEditDlg_run(MLK_WINDOW(p), &p->info);
			break;

		//開く
		case WID_BTT_OPEN:
			_cmd_open_file(p);
			break;
		//保存
		case WID_BTT_SAVE:
			_cmd_save_file(p);
			break;
		//終了
		case WID_BTT_EXIT:
			mGuiQuit();
			break;
	}
}

/* イベントハンドラ */

static int _event_handle(mWidget *wg,mEvent *ev)
{
	MainWindow *p = (MainWindow *)wg;

	switch(ev->type)
	{
		//通知
		case MEVENT_NOTIFY:
			_event_notify(p, (mEventNotify *)ev);
			break;
	
		//コマンド
		case MEVENT_COMMAND:
			_event_command(p, ev->cmd.id);
			break;

		//ファイルのドロップ
		case MEVENT_DROP_FILES:
			_open_file(p, *(ev->dropfiles.files));
			break;

		//終了
		case MEVENT_CLOSE:
			mGuiQuit();
			break;
	}

	return 1;
}


//===========================
// 作成
//===========================


/* ウィジェット作成 */

static void _create_widgets(MainWindow *p)
{
	mWidget *ctg,*ct;

	MLK_TRGROUP(TRGROUP_ID_MAINWIN);

	//------ 編集

	ctg = mContainerCreateGrid(MLK_WIDGET(p), 2, 7, 9, MLF_EXPAND_WH, 0);

	mContainerSetPadding_same(MLK_CONTAINER(ctg), 8);

	//フォント

	mLabelCreate(ctg, MLF_MIDDLE | MLF_RIGHT, 0, 0, MLK_TR(TRID_MAINWIN_FONT));

	ct = mContainerCreateHorz(ctg, 5, MLF_EXPAND_W, 0);

	p->fontbtt = mFontButtonCreate(ct, WID_FONTBTT, MLF_EXPAND_W, 0, 0);

	mFontButtonSetFlags(p->fontbtt, MSYSDLG_FONT_F_ALL);
	mFontButtonSetInfo(p->fontbtt, &p->info.fontinfo);
	
	mWidgetSetInitSize_fontHeightUnit(MLK_WIDGET(p->fontbtt), 16, 0);

	mButtonCreate(ct, WID_FONT_HELP, 0, 0, MBUTTON_S_REAL_W, "?");

	//アイコンテーマ

	mLabelCreate(ctg, MLF_MIDDLE | MLF_RIGHT, 0, 0, MLK_TR(TRID_MAINWIN_ICONTHEME));

	ct = mContainerCreateHorz(ctg, 5, MLF_EXPAND_W, 0);

	p->le_icontheme = mLineEditCreate(ct, 0, MLF_EXPAND_W | MLF_MIDDLE, 0, 0);

	mLineEditSetText(p->le_icontheme, p->info.strIcontheme.buf);

	mButtonCreate(ct, WID_ICONTHEME_SELECT, 0, 0, 0, MLK_TR(TRID_MAINWIN_SELECT));

	//色

	mLabelCreate(ctg, MLF_MIDDLE | MLF_RIGHT, 0, 0, MLK_TR(TRID_MAINWIN_COLOR));

	mButtonCreate(ctg, WID_COLOR_EDIT, 0, 0, 0, MLK_TR(TRID_MAINWIN_EDIT));

	//------ ボタン

	ct = mContainerCreateHorz(MLK_WIDGET(p), 5, MLF_EXPAND_W, 0);

	mContainerSetPadding_same(MLK_CONTAINER(ct), 8);

	mButtonCreate(ct, WID_BTT_OPEN, MLF_EXPAND_X | MLF_RIGHT, 0, 0, MLK_TR(TRID_MAINWIN_OPEN));
	mButtonCreate(ct, WID_BTT_SAVE, 0, 0, 0, MLK_TR(TRID_MAINWIN_SAVE));
	mButtonCreate(ct, WID_BTT_EXIT, 0, 0, 0, MLK_TR(TRID_MAINWIN_EXIT));
}

/* メニュー作成 */

static void _create_menu(MainWindow *p)
{
	mMenuBar *bar;

	MLK_TRGROUP(TRGROUP_ID_MAINMENU);

	bar = mMenuBarNew(MLK_WIDGET(p), 0, MMENUBAR_S_BORDER_BOTTOM);

	mToplevelAttachMenuBar(MLK_TOPLEVEL(p), bar);

	mMenuBarCreateMenuTrArray16(bar, g_mainmenu_dat);
}

/* 破棄ハンドラ */

static void _destroy_handle(mWidget *wg)
{
	MainWindow *p = (MainWindow *)wg;

	mStrFree(&p->strOpenDir);
	mStrFree(&p->strSaveDir);

	mGuiStyle_free(&p->info);
}

/* メインウィンドウ作成 */

static void _create_mainwin(void)
{
	MainWindow *p;

	//ウィンドウ
	
	p = (MainWindow *)mToplevelNew(NULL, sizeof(MainWindow),
		MTOPLEVEL_S_NORMAL | MTOPLEVEL_S_TAB_MOVE);
	if(!p) return;
	
	p->wg.event = _event_handle;
	p->wg.destroy = _destroy_handle;
	p->wg.fstate |= MWIDGET_STATE_ENABLE_DROP;

	mToplevelSetTitle(MLK_TOPLEVEL(p), "mlk style editor");

	//現在のスタイル取得

	mGuiStyle_getCurrentInfo(&p->info);

	//デフォルトのディレクトリ

	mStrPathSetHome_join(&p->strOpenDir, ".config");
	mStrPathSetHome_join(&p->strSaveDir, ".config");

	//作成

	_create_menu(p);

	_create_widgets(p);

	//表示

	mWindowResizeShow_initSize(MLK_WINDOW(p));
}


//===========================
// メイン
//===========================


/** メイン */

int main(int argc,char **argv)
{
	if(mGuiInit(argc, argv, NULL)) return 1;

	mGuiSetWMClass("mlk-style-editor", "mlk-style-editor");

	mGuiSetPath_data_exe("../share/mlkstyle");

	if(mGuiInitBackend()) return 1;

	mGuiLoadTranslation(g_deftransdat, NULL, NULL);

	//ファイルダイアログ:デフォルトで隠しファイル表示

	mGuiFileDialog_showHiddenFiles();

	//ウィンドウ作成

	_create_mainwin();

	//

	mGuiRun();

	mGuiEnd();

	return 0;
}

