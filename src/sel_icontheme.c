/*$
mlk style editor
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * アイコンテーマの選択
 **********************************/

#include <string.h>

#include "mlk_gui.h"
#include "mlk_widget_def.h"
#include "mlk_widget.h"
#include "mlk_window.h"
#include "mlk_event.h"
#include "mlk_pixbuf.h"
#include "mlk_listview.h"
#include "mlk_columnitem.h"
#include "mlk_dir.h"
#include "mlk_str.h"
#include "mlk_iniread.h"
#include "mlk_icontheme.h"
#include "mlk_imagelist.h"

#include "def.h"


//-------------------

typedef struct
{
	mWidget wg;

	mPixbuf *img;
}_preview;

typedef struct
{
	MLK_DIALOG_DEF

	mListView *list;
	_preview *prev;
}_dialog;

enum
{
	WID_LIST = 100
};

#define _ICON_SIZE    24
#define _ICON_COLNUM  5
#define _ICON_ROWNUM  2
#define _ICON_NUM     10

#define _PREVIEW_WIDTH  _ICON_SIZE * _ICON_COLNUM
#define _PREVIEW_HEIGHT _ICON_SIZE * _ICON_ROWNUM
#define _PREVIEW_BKGND  0x909090

//プレビューアイコン名
static const char *g_icon_names = 
	"document-new\0document-open\0document-save\0application-exit\0folder\0"
	"list-add\0edit-delete\0edit-select-all\0edit-copy\0edit-paste\0";

//-------------------


//============================
// プレビュー
//============================


/* イメージセット */

static void _preview_setimage(_preview *p,const char *name)
{
	mIconTheme theme;
	mImageList *img = NULL;
	int i;

	mPixbufFill(p->img, mRGBtoPix(_PREVIEW_BKGND));

	//イメージリスト作成

	theme = mIconTheme_loadTheme(name);
	if(theme)
	{
		img = mImageListCreate_icontheme(theme, g_icon_names, _ICON_SIZE);
		mIconTheme_free(theme);
	}

	//セット

	if(img)
	{
		for(i = 0; i < _ICON_NUM; i++)
		{
			mImageListPutPixbuf(img, p->img,
				(i % _ICON_COLNUM) * _ICON_SIZE, (i / _ICON_COLNUM) * _ICON_SIZE,
				i, 0);
		}

		mImageListFree(img);
	}

	mWidgetRedraw(MLK_WIDGET(p));
}

/* 描画 */

static void _preview_draw(mWidget *wg,mPixbuf *pixbuf)
{
	mPixbufBlt(pixbuf, 0, 0,
		((_preview *)wg)->img, 0, 0, -1, -1);
}

/* 破棄ハンドラ */

static void _preview_destroy(mWidget *wg)
{
	mPixbufFree(((_preview *)wg)->img);
}

/* プレビュー作成 */

static _preview *_create_preview(mWidget *parent)
{
	_preview *p;

	p = (_preview *)mWidgetNew(parent, sizeof(_preview));
	if(!p) return NULL;

	p->wg.destroy = _preview_destroy;
	p->wg.draw = _preview_draw;

	p->wg.flayout = MLF_FIX_WH;
	p->wg.w = _PREVIEW_WIDTH;
	p->wg.h = _PREVIEW_HEIGHT;

	//

	p->img = mPixbufCreate(_PREVIEW_WIDTH, _PREVIEW_HEIGHT, 0);

	mPixbufFill(p->img, mRGBtoPix(_PREVIEW_BKGND));

	return p;
}


//============================
// ハンドラ
//============================


/* イベントハンドラ */

static int _event_handle(mWidget *wg,mEvent *ev)
{
	_dialog *p = (_dialog *)wg;

	//リスト選択変更

	if(ev->type == MEVENT_NOTIFY
		&& ev->notify.id == WID_LIST
		&& ev->notify.notify_type == MLISTVIEW_N_CHANGE_FOCUS)
	{
		_preview_setimage(p->prev, MLK_COLUMNITEM(ev->notify.param1)->text);
		return 1;
	}

	return mDialogEventDefault_okcancel(wg, ev);
}


//============================
// 作成
//============================


/* 指定ディレクトリがアイコンテーマか */

static mlkbool _is_dir_icontheme(const char *path)
{
	mIniRead *ini;
	mlkbool ret = FALSE;

	if(mIniRead_loadFile_join(&ini, path, "index.theme") == MLKERR_OK)
	{
		ret = (mIniRead_setGroup(ini, "icon theme")
			&& mIniRead_getText(ini, "directories", NULL));
	}

	mIniRead_end(ini);

	return ret;
}

/* ディレクトリから、テーマ列挙 */

static void _find_theme(mListView *list,const char *path)
{
	mDir *dir;
	const char *name;
	mStr str = MSTR_INIT;

	dir = mDirOpen(path);
	if(!dir) return;

	while(mDirNext(dir))
	{
		if(mDirIsSpecName(dir)
			|| !mDirIsDirectory(dir))
			continue;

		name = mDirGetFilename(dir);
		if(strcmp(name, "hicolor") == 0) continue;

		mDirGetFilename_str(dir, &str, TRUE);

		if(_is_dir_icontheme(str.buf))
			mListViewAddItem_text_copy(list, name);
	}

	mDirClose(dir);

	mStrFree(&str);
}

/* リストをセット */

static void _set_list(mListView *list)
{
	mStr str = MSTR_INIT;

	_find_theme(list, "/usr/share/icons");

	_find_theme(list, "/usr/local/share/icons");

	mStrPathSetHome_join(&str, ".icons");
	_find_theme(list, str.buf);

	mStrPathSetHome_join(&str, ".local/share/icons");
	_find_theme(list, str.buf);

	mStrFree(&str);

	//

	mListViewSortItem(list, mColumnItem_sortFunc_strcmp, NULL);
	mListViewSetAutoWidth(list, TRUE);
}

/* ダイアログ作成 */

static _dialog *_create_dialog(mWindow *parent)
{
	_dialog *p;
	mWidget *ct;

	p = (_dialog *)mDialogNew(parent, sizeof(_dialog), MTOPLEVEL_S_DIALOG_NORMAL);
	if(!p) return NULL;

	p->wg.event = _event_handle;

	mToplevelSetTitle(MLK_TOPLEVEL(p), MLK_TR2(TRGROUP_ID_DIALOG, TRID_DLG_SELECT_ICONTHEME));

	mContainerSetPadding_same(MLK_CONTAINER(p), 8);

	ct = mContainerCreateHorz(MLK_WIDGET(p), 8, MLF_EXPAND_WH, 0);

	//リスト

	p->list = mListViewCreate(ct, WID_LIST, MLF_EXPAND_H, 0, 0, MSCROLLVIEW_S_HORZVERT_FRAME);

	mWidgetSetInitSize_fontHeightUnit(MLK_WIDGET(p->list), 0, 12);

	_set_list(p->list);

	//プレビュー

	p->prev = _create_preview(ct);

	//OK/キャンセル

	mContainerCreateButtons_okcancel(MLK_WIDGET(p), MLK_MAKE32_4(0,15,0,0));

	return p;
}

/** ダイアログ実行 */

mlkbool SelIconThemeDlg_run(mWindow *parent,mStr *strdst)
{
	_dialog *p;
	mColumnItem *pi;
	mlkbool ret = FALSE;

	p = _create_dialog(parent);
	if(!p) return FALSE;

	mWindowResizeShow_initSize(MLK_WINDOW(p));

	if(mDialogRun(MLK_DIALOG(p), FALSE))
	{
		pi = mListViewGetFocusItem(p->list);
		if(pi)
		{
			mStrSetText(strdst, pi->text);
			ret = TRUE;
		}
	}

	mWidgetDestroy(MLK_WIDGET(p));

	return ret;
}

