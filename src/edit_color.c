/*$
mlk style editor
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * 色の編集
 **********************************/

#include <stdio.h>

#include "mlk_gui.h"
#include "mlk_widget_def.h"
#include "mlk_widget.h"
#include "mlk_window.h"
#include "mlk_sysdlg.h"
#include "mlk_event.h"
#include "mlk_guicol.h"
#include "mlk_menu.h"
#include "mlk_font.h"
#include "mlk_pixbuf.h"
#include "mlk_imagelist.h"
#include "mlk_columnitem.h"
#include "mlk_str.h"
#include "mlk_guistyle.h"

#include "mlk_menubar.h"
#include "mlk_listview.h"
#include "mlk_button.h"
#include "mlk_checkbutton.h"
#include "mlk_colorbutton.h"
#include "mlk_scrollbar.h"
#include "mlk_iconbar.h"
#include "mlk_groupbox.h"
#include "mlk_combobox.h"
#include "mlk_lineedit.h"
#include "mlk_tab.h"
#include "mlk_sliderbar.h"
#include "mlk_progressbar.h"
#include "mlk_imgbutton.h"

#include "def.h"
#include "def_editcolor.h"


//-------------------

typedef struct
{
	MLK_DIALOG_DEF

	mListView *list;
	mColorButton *colbtt;

	mColumnItem *selitem;

	uint32_t col_copy;	//コピーした色
	int32_t	undo_col,	//アンドゥ色
			undo_no;	//アンドゥする色番号 (-1 でなし)
}_dialog;


#define LISTITEM_COLORBOX_W   20  //色リストの色部分の幅
#define LISTITEM_COLORBOX_SEP 3	  //色リストの色部分とテキスト間の余白

enum
{
	WID_LIST_COLOR = 100,
	WID_COLORBTT,
	WID_BTT_COPY,
	WID_BTT_PASTE,
	WID_BTT_UNDO
};

//-------------------


//===========================
// sub
//===========================


/* 全体の更新時 */

static void _update_full(_dialog *p)
{
	mColorButtonSetColor(p->colbtt, mGuiCol_getRGB(p->selitem->param));

	mGuiCol_RGBtoPix_all();
	mWidgetRedraw(MLK_WIDGET(p));
}

/* 選択色の変更時 */

static void _change_color(_dialog *p,uint32_t col)
{
	int no = p->selitem->param;

	p->undo_no = no;
	p->undo_col = mGuiCol_getRGB(no);

	mGuiCol_changeColor(no, col);

	mColorButtonSetColor(p->colbtt, col);

	mWidgetRedraw(MLK_WIDGET(p));
}

/* 色を元に戻す */

static void _undo_color(_dialog *p)
{
	if(p->undo_no == -1) return;

	mGuiCol_changeColor(p->undo_no, p->undo_col);

	if(p->selitem->param == p->undo_no)
		mColorButtonSetColor(p->colbtt, p->undo_col);

	p->undo_no = -1;

	mWidgetRedraw(MLK_WIDGET(p));
}

/* カラー用 conf を適用 */

static void _apply_color_conf(_dialog *p,const char *name)
{
	mStr str = MSTR_INIT;

	mGuiGetPath_data(&str, name);
	mStrAppendText(&str, ".conf");

	mGuiStyle_loadColor(str.buf);

	mStrFree(&str);

	_update_full(p);
}


//===========================
// ハンドラ
//===========================


/* コマンド */

static void _event_command(_dialog *p,int id)
{
	switch(id)
	{
		//色を初期値に戻す
		case TRID_COLMENU_EDIT_RESET:
			mGuiCol_setDefaultRGB();

			_update_full(p);
			break;
		//black カラーを適用
		case TRID_COLMENU_EDIT_BLACK:
			_apply_color_conf(p, "black");
			break;
	}
}

/* 通知 */

static void _event_notify(_dialog *p,mEventNotify *ev)
{
	int type = ev->notify_type;

	switch(ev->id)
	{
		//リスト
		case WID_LIST_COLOR:
			if(type == MLISTVIEW_N_CHANGE_FOCUS)
			{
				//選択変更

				p->selitem = (mColumnItem *)ev->param1;
				
				mColorButtonSetColor(p->colbtt, mGuiCol_getRGB(p->selitem->param));
			}
			else if(type == MLISTVIEW_N_ITEM_L_DBLCLK)
			{
				//ダブルクリック -> 色選択

				mRgbCol col;

				col = mGuiCol_getRGB(p->selitem->param);

				if(mSysDlg_selectColor(MLK_WINDOW(p), &col))
					_change_color(p, col);
			}
			break;

		//色ボタン
		case WID_COLORBTT:
			if(type == MCOLORBUTTON_N_PRESS)
				_change_color(p, ev->param1);
			break;

		//コピー
		case WID_BTT_COPY:
			p->col_copy = mGuiCol_getRGB(p->selitem->param);
			break;

		//貼り付け
		case WID_BTT_PASTE:
			_change_color(p, p->col_copy);
			break;
		//アンドゥ
		case WID_BTT_UNDO:
			_undo_color(p);
			break;
	}
}

/* イベントハンドラ */

static int _event_handle(mWidget *wg,mEvent *ev)
{
	_dialog *p = (_dialog *)wg;

	switch(ev->type)
	{
		//通知
		case MEVENT_NOTIFY:
			_event_notify(p, (mEventNotify *)ev);
			break;
	
		//コマンド
		case MEVENT_COMMAND:
			_event_command(p, ev->cmd.id);
			break;
	}

	return mDialogEventDefault_okcancel(wg, ev);
}


//===========================
// 描画ハンドラ
//===========================


/* 色リストのアイテム描画ハンドラ */

static int _listitem_draw_handle(mPixbuf *pixbuf,mColumnItem *item,mColumnItemDrawInfo *info)
{
	mFont *font = mWidgetGetFont(info->widget);
	mBox box;
	int n,h;

	box = info->box;
	h = box.h;

	//テキスト
	// :GUI色が変わると、テキストが見えなくなる場合があるため、固定色で影を付ける

	n = info->box.y + (h - mFontGetHeight(font)) / 2;

	mFontDrawText_pixbuf(font, pixbuf, info->box.x + 1, n, item->text, -1, 0xffffff);
	mFontDrawText_pixbuf(font, pixbuf, info->box.x, n, item->text, -1, 0);

	//色

	n = box.x + box.w - LISTITEM_COLORBOX_W;

	mPixbufBox(pixbuf, n, box.y + 1, LISTITEM_COLORBOX_W, h - 2, 0);

	mPixbufFillBox(pixbuf, n + 1, box.y + 2, LISTITEM_COLORBOX_W - 2, h - 4,
		mGuiCol_getPix(item->param));

	return 0;
}

/* mButton: フォーカス */

static void _drawhandle_btt_focus(mWidget *wg,mPixbuf *pixbuf)
{
	mButton *p = (mButton *)wg;

	mPixbufDrawButton(pixbuf, 0, 0, wg->w, wg->h, MPIXBUF_DRAWBTT_FOCUSED);

	mWidgetLabelText_draw(&p->btt.txt, pixbuf, mWidgetGetFont(wg),
		0, (wg->h - p->btt.txt.szfull.h) / 2, wg->w,
		MGUICOL_RGB(TEXT), MWIDGETLABELTEXT_DRAW_F_CENTER);
}

/* ツールチップ */

static void _drawhandle_tooltip(mWidget *wg,mPixbuf *pixbuf)
{
	//枠

	mPixbufBox(pixbuf, 0, 0, wg->w, wg->h, MGUICOL_PIX(TOOLTIP_FRAME));

	//背景

	mPixbufFillBox(pixbuf, 1, 1, wg->w - 2, wg->h - 2, MGUICOL_PIX(TOOLTIP_FACE));

	//テキスト

	mFontDrawText_pixbuf(mWidgetGetFont(wg), pixbuf,
		5, 3, "tooltip", -1, MGUICOL_RGB(TOOLTIP_TEXT));
}


//===========================
// 作成
//===========================


/* アイコンバー作成 */

static void _create_iconbar(mWidget *parent)
{
	mIconBar *ib;
	mImageList *img;

	ib = mIconBarNew(parent, 0,
		MICONBAR_S_TOOLTIP | MICONBAR_S_SEP_BOTTOM | MICONBAR_S_DESTROY_IMAGELIST);

	img = mImageListLoadPNG_buf(g_png_iconbtt, sizeof(g_png_iconbtt), 16);

	mImageListSetTransparentColor(img, 0x00ff00);

	//

	mIconBarSetImageList(ib, img);

	mIconBarSetTooltipTrGroup(ib, TRGROUP_ID_TOOLTIP);

	mIconBarAdd(ib, 0, 0, 0, 0);
	mIconBarAdd(ib, 1, 1, 0, MICONBAR_ITEMF_CHECKBUTTON | MICONBAR_ITEMF_CHECKED);
	mIconBarAddSep(ib);
	mIconBarAdd(ib, 2, 2, 0, MICONBAR_ITEMF_CHECKGROUP | MICONBAR_ITEMF_CHECKED);
	mIconBarAdd(ib, 3, 3, 0, MICONBAR_ITEMF_CHECKGROUP);
	mIconBarAddSep(ib);
	mIconBarAdd(ib, 4, 4, 0, MICONBAR_ITEMF_DISABLE);
	mIconBarAdd(ib, 5, 5, 0, MICONBAR_ITEMF_DROPDOWN);
}

/* サンプルウィジェット作成 */

static void _create_sample_widgets(_dialog *p,mWidget *parent)
{
	mWidget *ct_topv,*ct_h,*ct,*wg;
	mGroupBox *gb;
	mComboBox *cb;
	mTab *tab;
	mListView *lv;
	mFont *font;
	char m[16];
	int i,fonth;

	font = mWidgetGetFont(MLK_WIDGET(p));
	fonth = mFontGetHeight(font);

	ct_topv = mContainerCreateVert(parent, 10, MLF_EXPAND_WH, 0);

	//----- (上) アイコンバー

	_create_iconbar(ct_topv);

	//----- (下)

	ct_h = mContainerCreateHorz(ct_topv, 10, MLF_EXPAND_WH, 0);

	//--- (左) [group-vert]

	gb = mGroupBoxCreate(ct_h, 0, 0, 0, "group1");
	gb->ct.sep = 9;

	//button

	mButtonCreate(MLK_WIDGET(gb), 0, MLF_EXPAND_W, 0, 0, "Button");

	wg = (mWidget *)mButtonCreate(MLK_WIDGET(gb), 0, MLF_EXPAND_W, 0, 0, "default");
	wg->fstate |= MWIDGET_STATE_ENTER_SEND;

	//checkbutton

	mCheckButtonCreate(MLK_WIDGET(gb), 0, 0, 0, MCHECKBUTTON_S_BUTTON, "button press", 1);

	mCheckButtonCreate(MLK_WIDGET(gb), 0, 0, 0, 0, "Check Button", 1);

	//lineedit

	wg = (mWidget *)mLineEditCreate(MLK_WIDGET(gb), 0, MLF_EXPAND_W, 0, 0);

	mLineEditSetText(MLK_LINEEDIT(wg), "disable");

	mWidgetEnable(wg, 0);

	//combobox

	cb = mComboBoxCreate(MLK_WIDGET(gb), 0, 0, 0, 0);

	for(i = 1; i <= 6; i++)
	{
		snprintf(m, 16, "item%d", i);
		mComboBoxAddItem_copy(cb, m, 0);
	}

	mComboBoxSetAutoWidth(cb);
	mComboBoxSetSelItem_atIndex(cb, 0);

	//tab

	tab = mTabCreate(MLK_WIDGET(gb), 0, MLF_EXPAND_W, 0, MTAB_S_TOP | MTAB_S_HAVE_SEP);

	for(i = 1; i <= 3; i++)
	{
		snprintf(m, 16, "tab%d", i);
		mTabAddItem(tab, m, -1, MTABITEM_F_COPYTEXT, 0);
	}

	mTabSetSel_atIndex(tab, 0);

	//slider bar

	wg = (mWidget *)mSliderBarCreate(MLK_WIDGET(gb), 0, MLF_EXPAND_W, 0, 0);

	mSliderBarSetStatus(MLK_SLIDERBAR(wg), 0, 100, 50);

	//progress bar

	wg = (mWidget *)mProgressBarCreate(MLK_WIDGET(gb), 0, MLF_EXPAND_W, 0,
		MPROGRESSBAR_S_FRAME | MPROGRESSBAR_S_TEXT_PERS);

	mProgressBarSetStatus(MLK_PROGRESSBAR(wg), 0, 100, 50);

	//scroll bar

	wg = (mWidget *)mScrollBarCreate(MLK_WIDGET(gb), 0, MLF_EXPAND_W, 0, MSCROLLBAR_S_HORZ);

	mScrollBarSetStatus(MLK_SCROLLBAR(wg), 0, 100, 20);

	//--- (右)

	ct = mContainerCreateVert(ct_h, 9, MLF_EXPAND_WH, 0);

	//フォーカス枠

	wg = (mWidget *)mButtonCreate(ct, 0, MLF_EXPAND_W, 0, 0, "focus");
	wg->draw = _drawhandle_btt_focus;

	//ツールチップ

	wg = mWidgetNew(ct, 0);
	wg->draw = _drawhandle_tooltip;
	wg->hintW = mFontGetTextWidth(font, "tooltip", -1) + 10;
	wg->hintH = fonth + 6;

	//リスト

	lv = mListViewCreate(ct, 0, MLF_EXPAND_WH, 0,
		MLISTVIEW_S_GRID_ROW | MLISTVIEW_S_MULTI_SEL, MSCROLLVIEW_S_FRAME);

	mListViewAddItem(lv, "header", -1, MCOLUMNITEM_F_HEADER, 0);

	for(i = 0; i < 5; i++)
	{
		mListViewAddItem(lv, "item", -1, MCOLUMNITEM_F_SELECTED, 0);

		if(i == 0) mListViewSetFocusItem_index(lv, 1);
	}
}

/* ウィジェット作成 */

static void _create_widgets(_dialog *p)
{
	mWidget *ct_toph,*ct,*ct2,*wg;
	mListView *list;
	mColumnItem *pi;
	mFont *font;
	int i,w,maxw;

	ct_toph = mContainerCreateHorz(MLK_WIDGET(p), 15, MLF_EXPAND_WH, 0);

	mContainerSetPadding_same(MLK_CONTAINER(ct_toph), 8);

	//------ 色リスト

	font = mWidgetGetFont(MLK_WIDGET(p));

	ct = mContainerCreateVert(ct_toph, 6, MLF_EXPAND_H, 0);

	p->list = list = mListViewCreate(ct, WID_LIST_COLOR, MLF_EXPAND_H, 0,
		0, MSCROLLVIEW_S_VERT | MSCROLLVIEW_S_FRAME);

	//アイテム高さ

	i = mFontGetHeight(font) + 2;
	if(i < 16) i = 16;

	mListViewSetItemHeight_min(list, i);

	//アイテム追加

	maxw = 0;

	for(i = 0; i < MGUICOL_NUM - 1; i++)
	{
		pi = mListViewAddItem(list, g_color_name[i], -1, 0, i + 1);

		pi->draw = _listitem_draw_handle;

		w = mFontGetTextWidth(font, g_color_name[i], -1);
		if(maxw < w) maxw = w;
	}

	//

	p->selitem = mListViewSetFocusItem_index(list, 0);

	list->wg.hintRepW = mListViewCalcWidgetWidth(list, maxw) + LISTITEM_COLORBOX_W + LISTITEM_COLORBOX_SEP;

	//---- コマンドボタン

	ct2 = mContainerCreateHorz(ct, 5, MLF_EXPAND_W, 0);

	//コピー

	wg = (mWidget *)mImgButtonCreate(ct2, WID_BTT_COPY, 0, 0, 0);

	mImgButton_setBitImage(MLK_IMGBUTTON(wg), MIMGBUTTON_TYPE_2BIT_BLACK_TP_WHITE, g_img_copy, 16, 16);

	//貼り付け

	wg = (mWidget *)mImgButtonCreate(ct2, WID_BTT_PASTE, 0, 0, 0);

	mImgButton_setBitImage(MLK_IMGBUTTON(wg), MIMGBUTTON_TYPE_2BIT_BLACK_TP_WHITE, g_img_paste, 16, 16);

	//アンドゥ

	wg = (mWidget *)mImgButtonCreate(ct2, WID_BTT_UNDO, 0, 0, 0);

	mImgButton_setBitImage(MLK_IMGBUTTON(wg), MIMGBUTTON_TYPE_2BIT_BLACK_TP_WHITE, g_img_undo, 16, 16);

	//---- 色ボタン

	p->colbtt = mColorButtonCreate(ct, WID_COLORBTT, MLF_EXPAND_W, 0, MCOLORBUTTON_S_DIALOG, mGuiCol_getRGB(1));

	//------ サンプル

	_create_sample_widgets(p, ct_toph);
}

/* メニュー作成 */

static void _create_menu(_dialog *p)
{
	mMenuBar *bar;
	mMenu *menu;

	MLK_TRGROUP(TRGROUP_ID_EDITCOLOR_MENU);

	bar = mMenuBarNew(MLK_WIDGET(p), 0, MMENUBAR_S_BORDER_BOTTOM);

	mToplevelAttachMenuBar(MLK_TOPLEVEL(p), bar);

	mMenuBarCreateMenuTrArray16(bar, g_colmenu_dat);

	//

	menu = mMenuBarGetMenu(bar);

	mMenuSetItemShortcutKey(menu, TRID_COLMENU_TEST_ITEM1, MLK_ACCELKEY_CTRL + 'I');
	mMenuSetItemEnable(menu, TRID_COLMENU_TEST_ITEM2, 0);
}

/* 破棄ハンドラ */

static void _destroy_handle(mWidget *wg)
{
	mFontFree(wg->font);
	wg->font = NULL;
}

/* ダイアログ作成 */

static _dialog *_create_dialog(mWindow *parent,mGuiStyleInfo *info)
{
	_dialog *p;

	p = (_dialog *)mDialogNew(parent, sizeof(_dialog), MTOPLEVEL_S_DIALOG_NORMAL);
	if(!p) return NULL;

	p->wg.destroy = _destroy_handle;
	p->wg.event = _event_handle;

	p->undo_no = -1;

	mToplevelSetTitle(MLK_TOPLEVEL(p), MLK_TR2(TRGROUP_ID_DIALOG, TRID_DLG_EDIT_COLOR));

	//フォント (フォント変更の適用を確認するため)

	p->wg.font = mFontCreate(mGuiGetFontSystem(), &info->fontinfo);

	//
	
	_create_menu(p);

	_create_widgets(p);

	//OK/キャンセル

	mContainerCreateButtons_okcancel(MLK_WIDGET(p), MLK_MAKE32_4(8,8,8,8));

	return p;
}

/** ダイアログ実行 */

void ColorEditDlg_run(mWindow *parent,mGuiStyleInfo *info)
{
	_dialog *p;

	p = _create_dialog(parent, info);
	if(!p) return;

	mGuiStyle_setColor(info);

	mWindowResizeShow_initSize(MLK_WINDOW(p));

	if(mDialogRun(MLK_DIALOG(p), TRUE))
		//現在のGUI色を取得
		mGuiStyle_getCurrentColor(info);
	else
		//色を元に戻す
		mGuiStyle_setColor(info);

	mWidgetRedraw(MLK_WIDGET(parent));
}

