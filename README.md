# mlk style editor

http://azsky2.html.xdomain.jp/

自作の GUI 構築ライブラリ mlk 用の、GUI スタイルエディタです。

フォント、アイコンテーマ、GUI の色を編集します。

## 動作環境

- Linux、macOS(要XQuartz) ほか
- X11

## コンパイル/インストール

ninja、pkg-config コマンドが必要です。<br>
各開発用ファイルのパッケージも必要になります。ReadMe を参照してください。

~~~
$ ./configure
$ cd build
$ ninja
# ninja install
~~~
